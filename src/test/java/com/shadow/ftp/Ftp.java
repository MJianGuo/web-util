package com.shadow.ftp;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.InetSocketAddress;
import java.net.SocketAddress;
import java.util.Iterator;

import org.apache.log4j.Logger;
import org.dom4j.Document;
import org.dom4j.DocumentException;
import org.dom4j.DocumentHelper;
import org.dom4j.Element;
import org.junit.Test;

import sun.net.ftp.FtpClient;
import sun.net.ftp.FtpProtocolException;

public class Ftp {
	private static Logger log = Logger.getLogger("logger");

	public static String IP = "static.upload.58dns.org";
	public static int PORT = 21;
	public static String USERNAME = "zhuanzhuanftp";
	public static String USERPSWD = "Urd4k]ez5Q";

	private String localfilename;

	private String remotefilename;

	private FtpClient ftpClient;

	public void startFtp() {
		// :TEST PATH
		// connectServer("127.0.0.1", 21, "name", "pswd", "/" );
		connectServer(IP, PORT, USERNAME, USERPSWD, "/");
	}

	public void connectServer(String ip, int port, String user, String password, String path) {
		try {
			ftpClient = FtpClient.create();
			try {
				SocketAddress addr = new InetSocketAddress(ip, port);
				ftpClient.connect(addr);
				ftpClient.login(user, password.toCharArray());
				System.out.println("login success!");
				if (path.length() != 0) {
					ftpClient.changeDirectory(path);
				}
			} catch (FtpProtocolException e) {

				e.printStackTrace();
			}
		} catch (IOException ex) {
			ex.printStackTrace();
			throw new RuntimeException(ex);
		}
	}

	public void closeConnect() {
		try {
			ftpClient.close();
			System.out.println("disconnect success");
		} catch (IOException ex) {
			System.out.println("not disconnect");
			ex.printStackTrace();
			throw new RuntimeException(ex);
		}
	}

	public void upload(String localFile, String remoteFile) {
		this.localfilename = localFile;
		this.remotefilename = remoteFile;
		OutputStream os = null;
		FileInputStream is = null;
		try {
			try {
				os = ftpClient.putFileStream(this.remotefilename);
			} catch (FtpProtocolException e) {

				e.printStackTrace();
			}
			File file_in = new File(this.localfilename);
			is = new FileInputStream(file_in);
			byte[] bytes = new byte[1024];
			int c;
			while ((c = is.read(bytes)) != -1) {
				os.write(bytes, 0, c);
			}
			System.out.println("upload success");
		} catch (IOException ex) {
			System.out.println("not upload");
			ex.printStackTrace();
			throw new RuntimeException(ex);
		} finally {
			try {
				if (is != null) {
					is.close();
				}
			} catch (IOException e) {
				e.printStackTrace();
			} finally {
				try {
					if (os != null) {
						os.close();
					}
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
		}
	}

	public String readFileStr(String remoteFile) {
		InputStream is = null;
		ByteArrayOutputStream baos = new ByteArrayOutputStream();
		try {
			try {
				is = ftpClient.getFileStream(remoteFile);
			} catch (FtpProtocolException e) {

				e.printStackTrace();
			}
			int i = -1;
			while ((i = is.read()) != -1) {
				baos.write(i);
			}
			System.out.println("readFileStr success");
		} catch (IOException ex) {
			System.out.println("not readFileStr");
			ex.printStackTrace();
			throw new RuntimeException(ex);
		} finally {
			try {
				if (is != null) {
					is.close();
				}
			} catch (IOException e) {
				e.printStackTrace();
			} finally {
			}
		}
		return (baos.toString());
	}

	public void download(String remoteFile, String localFile) {
		InputStream is = null;
		FileOutputStream os = null;
		try {
			try {
				is = ftpClient.getFileStream(remoteFile);
			} catch (FtpProtocolException e) {

				e.printStackTrace();
			}
			File file_in = new File(localFile);
			os = new FileOutputStream(file_in);
			byte[] bytes = new byte[1024];
			int c;
			while ((c = is.read(bytes)) != -1) {
				os.write(bytes, 0, c);
			}
			System.out.println("download success");
		} catch (IOException ex) {
			System.out.println("not download");
			ex.printStackTrace();
			throw new RuntimeException(ex);
		} finally {
			try {
				if (is != null) {
					is.close();
				}
			} catch (IOException e) {
				e.printStackTrace();
			} finally {
				try {
					if (os != null) {
						os.close();
					}
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
		}
	}

	
	@Test
	public void test(){
		
		Ftp ftp = new Ftp();
		ftp.startFtp();
		
		ftp.upload("d:/workbook.xls", "/banner333/abhue.xls");
	}
	public static void main(String agrs[]) throws DocumentException {
		String filepath[] = { "/20160704/12.xml", "/20160704/23.xml", "/20160704/45.xml" };
		String localfilepath[] = { "D://xml//dev_customer_type.xml", "D://xml//dev_edi_party_group.xml",
				"D://xml//dev_market_level.xml" };
		Ftp fu = new Ftp();
		fu.startFtp();
		String str = fu.readFileStr("/20160704/12.xml");
		fu.closeConnect();
		Document doc = DocumentHelper.parseText(str);
		Element rootElt = doc.getRootElement(); // 获取根节点
		for (Iterator i = rootElt.elementIterator("ROW"); i.hasNext();) {
			Element foo = (Element) i.next();
			System.out.println(foo.element("MARKET_LEVEL_ID").getTextTrim());
			System.out.println(foo.element("MARKET_LEVEL").getTextTrim());
			if (foo.element("NOTES") != null)
				System.out.println(foo.element("NOTES").getTextTrim());
		}
		// for ( int i = 0; i < filepath.length; i++ )
		// {
		// fu.download( filepath[i], localfilepath[i] );
		// }
		//
	}
}