package com.shadow.test;

import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.text.ParseException;
import java.util.Locale;

import org.junit.Test;

public class TestDecimalFormat {

	@Test
	public void test() {

		// 圆周率
		double pi = 3.1415926;

		// 取一位整数
		System.out.println(new DecimalFormat("0").format(pi)); // 3
		// 取一位整数和两位小数
		System.out.println(new DecimalFormat("0.00").format(pi));// 3.14
		// 取两位整数和三位小数，整数不足则以 0 填补
		System.out.println(new DecimalFormat("00.000").format(pi));// 03.142
		// 取所有的整数部分
		System.out.println(new DecimalFormat("#").format(pi));// 3
		// 以百分比方式计数，并取两位小数
		System.out.println(new DecimalFormat("#.##%").format(pi));// 314.16%
		System.out.println(new DecimalFormat("#.##\u2030").format(pi));// 3141.59‰
		System.out.println(new DecimalFormat("#.##(\u00A4)").format(pi));// 3.14(￥)
		System.out.println(new DecimalFormat("#.##¤").format(pi));// 3.14￥

		// 光速
		long c = 299792458;

		// 显示科学计数法，并取5位小数
		System.out.println(new DecimalFormat("#.#####E0").format(c));// 2.99792E8
		// 显示为两位整数的科学计数法，并且取四位小数
		System.out.println(new DecimalFormat("00.####E0").format(c));// 29.9792E7
		// 没三位以逗号进行分隔
		System.out.println(new DecimalFormat(",###").format(c));// 299,792,458
		// 将格式嵌入文本
		System.out.println(new DecimalFormat("光速大小为每秒,###米 。").format(c));// 光速大小为每秒299,792,458米
																			// 。

	}

	@Test
	public void test02() {
		Locale[] locales = NumberFormat.getAvailableLocales();
		double myNumber = -1234.56;
		NumberFormat form;
		for (int j = 0; j < 4; ++j) {
			System.out.println("FORMAT");
			for (int i = 0; i < locales.length; ++i) {
				if (locales[i].getCountry().length() == 0) {
					continue; // Skip language-only locales
				}
				System.out.print(locales[i].getDisplayName());
				switch (j) {
				case 0:
					form = NumberFormat.getInstance(locales[i]);
					break;
				case 1:
					form = NumberFormat.getIntegerInstance(locales[i]);
					break;
				case 2:
					form = NumberFormat.getCurrencyInstance(locales[i]);
					break;
				default:
					form = NumberFormat.getPercentInstance(locales[i]);
					break;
				}
				if (form instanceof DecimalFormat) {
					System.out.print(": " + ((DecimalFormat) form).toPattern());
				}
				System.out.print(" -> " + form.format(myNumber));
				try {
					System.out.println(" -> "
							+ form.parse(form.format(myNumber)));
				} catch (ParseException e) {
				}
			}
		}

	}
}
