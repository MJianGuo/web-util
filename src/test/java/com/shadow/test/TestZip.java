package com.shadow.test;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.zip.ZipEntry;
import java.util.zip.ZipInputStream;
import java.util.zip.ZipOutputStream;

import org.junit.Test;

import com.shadow.util.FileUtil;
import com.shadow.util.IOUtil;
import com.shadow.util.ZipUtil;

public class TestZip {

	/**
	 * 压缩一个文件
	 * 
	 * @throws IOException
	 */
	@Test
	public void testCompressFile() throws IOException {

		String file = "F:/aa/aa/bb/学生表.xls";
		String zip = "F:dir.zip";

		File dirFile = new File(file);
		System.out.println(dirFile.getAbsolutePath());
		System.out.println(dirFile.exists());
		InputStream inputStream = new FileInputStream(dirFile);

		File zipFile = new File(zip);
		OutputStream outputStream = new FileOutputStream(zipFile);
		ZipOutputStream zipOutputStream = new ZipOutputStream(outputStream);

		zipOutputStream.putNextEntry(new ZipEntry(dirFile.getName()));
		// zipOutputStream.putNextEntry(new ZipEntry(dirFile.getPath()));

		byte[] buffer = new byte[4096];
		int length = 0;
		while ((length = inputStream.read(buffer)) != -1) {
			zipOutputStream.write(buffer, 0, length);
		}

		zipOutputStream.flush();
		zipOutputStream.closeEntry();
		zipOutputStream.close();

		inputStream.close();

	}

	/**
	 * 解压缩
	 * 
	 * @throws IOException
	 */
	@Test
	public void testDecompression() throws IOException {

		String zip = "F:dir.zip";
		String fileDir = "F:dir";

		FileUtil.createDirs(fileDir);

		InputStream inputStream = new FileInputStream(zip);
		ZipInputStream zipInputStream = new ZipInputStream(inputStream);

		OutputStream outputStream = null;
		ZipEntry zipEntry = null;
		File file = null;
		while ((zipEntry = zipInputStream.getNextEntry()) != null
				&& !zipEntry.isDirectory()) {

			file = new File(fileDir + File.separator + zipEntry.getName());
			outputStream = new FileOutputStream(file);

			int length = 0;
			byte[] buffer = new byte[4096];
			while ((length = zipInputStream.read(buffer)) != -1) {
				outputStream.write(buffer, 0, length);
			}

			outputStream.flush();
		}

		zipInputStream.closeEntry();
		IOUtil.close(zipInputStream, inputStream);
	}

	@Test
	public void testZipUtil() {

		String dir = "F:/a/a/a/20170208173914";
		String zipFileName = "test.zip";
		
		String compress = ZipUtil.compress(dir, zipFileName);
		System.out.println(compress);
	}
}
