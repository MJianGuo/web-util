package com.shadow.test;

import java.lang.annotation.Annotation;
import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import javassist.ClassClassPath;
import javassist.ClassPool;
import javassist.CtClass;
import javassist.bytecode.AnnotationsAttribute;
import javassist.bytecode.ClassFile;
import javassist.bytecode.ConstPool;
import javassist.bytecode.annotation.StringMemberValue;

import org.junit.Test;

import com.shadow.annotation.ExcelAnnotation;
import com.shadow.model.User;

public class TestAnnotation {

	@Test
	public void testExcelAnnotation() throws NoSuchMethodException,
			SecurityException, IllegalAccessException,
			IllegalArgumentException, InvocationTargetException {

		User user = new User();
		user.setName("小明");
		user.setAge(43);

		Class<? extends User> clazz = user.getClass();

		/*
		 * 获取类上的注解
		 */
		Annotation[] annotations = clazz.getAnnotations();
		System.out.println(annotations.length);
		for (Annotation annotation : annotations) {
			System.out.println(annotation);
		}

		/*
		 * 获取类上指定类型的注解
		 */
		ExcelAnnotation annotation = clazz.getAnnotation(ExcelAnnotation.class);
		System.out.println("类  " + annotation);

		/*
		 * public 字段
		 */
		Field[] fields = clazz.getFields();
		System.out.println("字段   " + fields.length);
		for (Field field : fields) {

			/*
			 * 字段上的注解
			 */
			Annotation[] fielddAnnotationArr = field.getAnnotations();
			System.out.println("字段上  " + fielddAnnotationArr.length);
			for (Annotation fieldAnnotation : fielddAnnotationArr) {
				System.out.println(fieldAnnotation);
			}
		}

		List<ExcelAnnotation> list = new ArrayList<ExcelAnnotation>();
		/*
		 * 所有字段
		 */
		Field[] declaredFields = clazz.getDeclaredFields();
		for (Field declaredField : declaredFields) {
			ExcelAnnotation declaredFieldAnnotation = declaredField
					.getAnnotation(ExcelAnnotation.class);
			System.out.println(declaredFieldAnnotation);

			list.add(declaredFieldAnnotation);

		}

		Collections.sort(list, new Comparator<ExcelAnnotation>() {

			@Override
			public int compare(ExcelAnnotation annotation1,
					ExcelAnnotation annotation2) {

				return annotation1.orderNum() - annotation2.orderNum();
			}
		});

		for (ExcelAnnotation excelAnnotation : list) {
			System.out.println(excelAnnotation.rowHeader());
			System.out.println(excelAnnotation.fieldName());
		}

	}

	/*
	 * 修改注解的值
	 */
	@Test
	public void testAnnotation2() {/*
		// 从实现的角度说，ClassPool是一个CtClass对象的hash表，类名做为key。
		//ClassPool的get()搜索hash表找到与指定key关联的CtClass对象。
		ClassPool classPool = ClassPool.getDefault();
		classPool.appendClassPath(new ClassClassPath(User.class));
		// 如果CtClass通过writeFile(),toClass(),toBytecode()转换了类文件，javassist冻结了CtClass对象。
		// 以后是不允许修改这个 CtClass对象。这是为了警告开发人员当他们试图修改一个类文件时，已经被JVM载入的类不允许被重新载入。
		CtClass clazz = classPool.get(User.class.getName());
		clazz.stopPruning(true);
		// Defrost()执行后，CtClass对象将可以再次修改。
		clazz.defrost();
		ClassFile classFile = clazz.getClassFile();

		ConstPool constPool = classFile.getConstPool();
		Annotation tableAnnotation = new Annotation(, constPool);
		tableAnnotation.addMemberValue("name", new StringMemberValue(tableName, constPool));
		// 获取运行时注解属性  
		AnnotationsAttribute attribute = (AnnotationsAttribute) classFile.getAttribute(AnnotationsAttribute.visibleTag);
		attribute.addAnnotation(tableAnnotation);
		classFile.addAttribute(attribute);
		classFile.setVersionToJava5();
		// 当前ClassLoader中必须尚未加载该实体。（同一个ClassLoader加载同一个类只会加载一次）
		EntityClassLoader loader = new EntityClassLoader(ClassPoolUtils.class.getClassLoader());
		c = clazz.toClass(loader, null);
	*/
		
/*		 ClassFile cf = ;
		 ConstPool cp = cf.getConstPool();
		 AnnotationsAttribute attr
		     = new AnnotationsAttribute(cp, AnnotationsAttribute.visibleTag);
		 Annotation a = new Annotation("Author", cp);
		 a.addMemberValue("name", new StringMemberValue("Chiba", cp));
		 attr.setAnnotation(a);
		 cf.addAttribute(attr);
		 cf.setVersionToJava5();*/
	}
	
	
	public static void main(String[] args) throws ClassNotFoundException, InstantiationException, IllegalAccessException {
		Class<ExcelAnnotation> clazz = (Class<ExcelAnnotation>) Class.forName("com.shadow.annotation.ExcelAnnotation");
	
		System.out.println(clazz);
		clazz.newInstance();
	
	}
}
