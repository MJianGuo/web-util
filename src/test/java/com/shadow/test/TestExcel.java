package com.shadow.test;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.Calendar;
import java.util.Date;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;

import org.apache.poi.hssf.usermodel.HSSFCell;
import org.apache.poi.hssf.usermodel.HSSFCellStyle;
import org.apache.poi.hssf.usermodel.HSSFDateUtil;
import org.apache.poi.hssf.usermodel.HSSFRichTextString;
import org.apache.poi.hssf.usermodel.HSSFRow;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.hssf.util.HSSFColor;
import org.apache.poi.poifs.filesystem.POIFSFileSystem;
import org.apache.poi.ss.usermodel.CellStyle;
import org.apache.poi.ss.usermodel.RichTextString;
import org.junit.Before;
import org.junit.Test;

import com.shadow.callable.ExcelCallable;
import com.shadow.model.ExcelModel;
import com.shadow.model.StudentExcel;
import com.shadow.model.User;
import com.shadow.util.ExcelUtil;

public class TestExcel {

	String[] sheetNameArr = null;
	String[][] headerArr = null;

	@Before
	public void before() {

		sheetNameArr = new String[] { "学校", "年级", "班级" };
		headerArr = new String[][] { { "学校名称", "建校时间", "人数" },
				{ "年级名称", "人数" }, { "班级名称", "人数" } };
	}

	@Test
	public void testCreateExcel01() throws IOException {

		String filePath = "F:/test.xls";
		File file = new File(filePath);
		OutputStream fileOutputStream = new FileOutputStream(file);

		// 1.创建一个代表 excel 文件的对象
		HSSFWorkbook hSSFWorkbook = new HSSFWorkbook();

		// 2.创建一个sheet 工作表单
		HSSFSheet hSSFSheet = hSSFWorkbook.createSheet("第一个sheet");
		// 3获取sheet 表单的第一行
		HSSFRow hSSFRow = hSSFSheet.createRow(0);

		// 样式
		CellStyle cellStyle = hSSFWorkbook.createCellStyle();
		// 左右上下边框样式
		cellStyle.setBorderLeft(HSSFCellStyle.BORDER_THIN);
		cellStyle.setBorderRight(HSSFCellStyle.BORDER_THIN);
		cellStyle.setBorderTop(HSSFCellStyle.BORDER_THIN);
		cellStyle.setBorderBottom(HSSFCellStyle.BORDER_THIN);

		// 左右上下边框颜色(深蓝色),只有设置了上下左右边框以后给边框设置颜色才会生效
		cellStyle.setLeftBorderColor(HSSFColor.BLACK.index);
		cellStyle.setRightBorderColor(HSSFColor.BLACK.index);
		cellStyle.setTopBorderColor(HSSFColor.BLACK.index);
		cellStyle.setBottomBorderColor(HSSFColor.BLACK.index);

		hSSFRow.setRowStyle(cellStyle);

		// 4.获取第一行中第一个单元格
		HSSFCell hSSFCel = hSSFRow.createCell(0); // (0x0)
		// 5. 设置单元格的内容
		hSSFCel.setCellValue(true);
		hSSFCel.setCellStyle(cellStyle);

		// 重复 4 。5
		hSSFCel = hSSFRow.createCell(1);
		hSSFCel.setCellValue(Calendar.getInstance());

		hSSFCel = hSSFRow.createCell(2);
		hSSFCel.setCellValue(new Date());
		hSSFCel = hSSFRow.createCell(3);
		double dd = 2.515D;
		hSSFCel.setCellValue(dd);
		hSSFCel = hSSFRow.createCell(4);
		RichTextString richTextString = new HSSFRichTextString(
				"这是一个特殊格式的 RichTextString 字符串");
		hSSFCel.setCellValue(richTextString);
		hSSFCel = hSSFRow.createCell(5);
		hSSFCel.setCellValue("这是字符串");

		hSSFWorkbook.write(fileOutputStream);

	}

	@Test
	public void testCreateExcel() throws IOException {

		// 0.设置文件的存储路径和文件名称
		String filePath = "F:/test.xls";
		File file = new File(filePath);
		OutputStream fileOutputStream = new FileOutputStream(file);

		// 1.创建一个代表 excel 文件的对象
		HSSFWorkbook hSSFWorkbook = new HSSFWorkbook();

		String sheetName = null;

		for (int i = 0, length = sheetNameArr.length; i < length; i++) {

			sheetName = sheetNameArr[i];
			// 2.创建 excel 的工作表 sheet 对象
			HSSFSheet hSSFSheet = hSSFWorkbook.createSheet(sheetName);

			// 3.得到 某个 sheet 工作表的第一行，用于设置表头
			HSSFRow hSSFRow = hSSFSheet.createRow(0);

			String[] header = headerArr[i];
			HSSFCell hSSFCell = null;
			for (int j = 0, size = header.length; j < size; j++) {

				// 4.得到第一行的某个单元格进行表头内容的设置
				hSSFCell = hSSFRow.createCell(j);
				hSSFCell.setCellValue(header[j]);
			}

		}
		hSSFWorkbook.write(fileOutputStream);

	}

	@Test
	public void testReadExcel() throws IOException {
		// 0.定义要读取的 excel 文件
		String filePath = "F:/a/a/user.xls";
		File file = new File(filePath);
		InputStream fileInputStream = new FileInputStream(file);
		POIFSFileSystem pOIFSFileSystem = new POIFSFileSystem(fileInputStream);

		// 1.将 excel 对象与输入流进行关联，把 excel 文件加载一个 HSSFWorkbook 工作薄对象，这是解析的基础
		HSSFWorkbook hSSFWorkbook = new HSSFWorkbook(pOIFSFileSystem);

		/*
		 * 2.获取最后一个可用的 sheet 表单的角标值 （activeSheetIndex+1） 就是这个 excel 文件中总共的可用的
		 * sheet 表单
		 */
		int activeSheetIndex = hSSFWorkbook.getActiveSheetIndex();
		System.out.println("sheetNum : " + (activeSheetIndex + 1));
		HSSFSheet hSSFSheet = null;
		for (int i = 0; i <= activeSheetIndex; i++) {

			hSSFSheet = hSSFWorkbook.getSheetAt(i);

			/*
			 * 3.获取该 sheet 表单的可用的最后的行数
			 */
			int lastRowNum = hSSFSheet.getLastRowNum();
			System.out.println("lastRowNum:   " + lastRowNum);
			HSSFRow row = null;
			short lastCellNum = 0;
			for (int j = 0; j < lastRowNum; j++) {

				row = hSSFSheet.getRow(j);
				lastCellNum = row.getLastCellNum();
				HSSFCell cell = null;
				for (int k = 0; k < lastCellNum; k++) {
					cell = row.getCell(k);
					int cellType = cell.getCellType();

					int a = cell.LAST_COLUMN_NUMBER;
					System.out.println(cell.getStringCellValue());
				}

			}
		}

	}

	@Test
	public void testReadExcel2() throws IOException {
		// 0.定义要读取的 excel 文件
		String filePath = "F:/a/a/user.xls";
		File file = new File(filePath);
		InputStream fileInputStream = new FileInputStream(file);
		POIFSFileSystem pOIFSFileSystem = new POIFSFileSystem(fileInputStream);

		// 1.将 excel 对象与输入流进行关联，把 excel 文件加载一个 HSSFWorkbook 工作薄对象，这是解析的基础
		HSSFWorkbook hSSFWorkbook = new HSSFWorkbook(pOIFSFileSystem);

		HSSFSheet hSSFSheet = hSSFWorkbook.getSheetAt(0);
		HSSFRow row = hSSFSheet.getRow(2);
		short lastCellNum = row.getLastCellNum();
		HSSFCell hSSFCell = null;
		int cellType = 0;
		for (int i = 0; i < lastCellNum; i++) {
			hSSFCell = row.getCell(i);
			cellType = hSSFCell.getCellType();
		
			if(HSSFCell.CELL_TYPE_BLANK == cellType){
				
				
			}else if(HSSFCell.CELL_TYPE_STRING == cellType){
				String stringCellValue = hSSFCell.getStringCellValue();
				System.out.println(stringCellValue);
				
				HSSFRichTextString richStringCellValue = hSSFCell.getRichStringCellValue();
				System.out.println(richStringCellValue.toString());
			}else if(HSSFCell.CELL_TYPE_NUMERIC == cellType){
				
				double numericCellValue = hSSFCell.getNumericCellValue();
				if(HSSFDateUtil.isCellDateFormatted(hSSFCell)){
					Date javaDate = HSSFDateUtil.getJavaDate(numericCellValue);
					Date dateCellValue = hSSFCell.getDateCellValue();
					System.out.println(javaDate);
					System.out.println(dateCellValue);
				}
				System.out.println(numericCellValue);
			}else if(HSSFCell.CELL_TYPE_BOOLEAN == cellType){
				boolean booleanCellValue = hSSFCell.getBooleanCellValue();
				System.out.println(booleanCellValue);
			}else if(HSSFCell.CELL_TYPE_ERROR == cellType){
				byte errorCellValue = hSSFCell.getErrorCellValue();
				System.out.println(errorCellValue);
			}else{
				
			}
			
		}

	}

	public static void main(String[] args) throws IOException {

		ExcelModel<User> excelModel = new ExcelModel<User>();

		// excelModel.setSheetContentDataList(sheetContentDataList);
		// excelModel.setSheetList(sheetList);

		ExecutorService excutorService = Executors.newFixedThreadPool(1);

		Future<HSSFWorkbook> submit = excutorService
				.submit(new ExcelCallable<User>(excelModel));

		excutorService.shutdown();
		System.out.println("==================");
	}
}
