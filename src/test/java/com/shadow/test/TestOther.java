package com.shadow.test;

import java.io.File;
import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.text.SimpleDateFormat;
import java.time.Instant;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.junit.Test;

import com.shadow.model.User;

public class TestOther {

	@Test
	public void testFor() {

		int size = 1000000;
		List<User> userList = new ArrayList<User>(size);
		for (int i = 0; i < size; i++) {
			userList.add(new User());
		}

		long start1 = System.currentTimeMillis();
		for (int i = 0; i < size; i++) {
			userList.get(i);
		}
		long end1 = System.currentTimeMillis();

		System.out.println("===========" + (end1 - start1));

		long start2 = System.currentTimeMillis();

		for (User user : userList) {

		}
		long end2 = System.currentTimeMillis();
		System.out.println("===================" + (end2 - start2));

	}

	@Test
	public void test01() {

		Date now = new Date();
		String format = new SimpleDateFormat("yyyyMMddHH").format(now);
		String format2 = DateTimeFormatter.ofPattern("yyyyMMddHH")
				.format(LocalDateTime.ofInstant(Instant.ofEpochMilli(now.getTime()), ZoneId.systemDefault()));

		System.out.println(format);
		System.out.println(format2);

	}

	@Test
	public void test02() {

		String[] unit = { "B", "KB", "MB", "GB", "TB", "EB", "ZB", "YB", "BB" };
		byte[] buffer = new byte[1];
		int length = Integer.MAX_VALUE;
		double size = 0.0D;
		size = length / 1024;
		if (size == 0.0D) {
			System.out.println(length + "B");
		}
		int unitIndex = 1;
		while (size > 1024) {
			size = size / 1024;
			unitIndex++;
		}
		String.format("%.2f", size);

		System.out.println(String.format("%.2f", size));
		System.out.println(size + " " + unit[unitIndex]);
		
		
		File file = new File("d:/workbook.xls");
		System.out.println(file.length());
		System.out.println(computFileSize(file.length()));
	}

	public String computFileSize(long fileByteLength) {
		String[] unit = { "B", "KB", "MB", "GB", "TB", "EB", "ZB", "YB", "BB" };
		double size = 0.0D;
		size = fileByteLength / 1024;
		int unitIndex = 0;
		if (size == 0.0D) {
			return String.format("%.2f", size) + unit[unitIndex];
		}
		unitIndex++;
		while (size > 1024) {
			size = size / 1024;
			unitIndex++;
		}
		return String.format("%.2f", size) + unit[unitIndex];
	}
	
	@Test
	public void test03() throws UnsupportedEncodingException{
		
		String encode = URLEncoder.encode("工单明细-20180510105632.xlsx");
		
		System.out.println(encode);
		
		encode = URLEncoder.encode("工单明细-20180510105632.xlsx","gbk");
		System.out.println(encode);
		encode = URLEncoder.encode("工单明细-20180510105632.xlsx","utf-8");
		System.out.println(encode);
	}
	
	@Test
	public void test04(){
		Long a = 1L;
		System.out.println(a.equals(1L));
		System.out.println(a.equals(1));

		
		Integer b = 1;
		System.out.println(b.equals(1L));
		System.out.println(b.equals(1));
	}
}
