package com.shadow.test;

import java.util.ArrayList;
import java.util.List;

import org.junit.Test;

import com.shadow.model.User;

public class TestMath {

	/**
	 * 将 list 集合切成每份指定的元素的数 总分数不确定
	 */
	@Test
	public void testSubList() {

		int size = 4;
		List<User> list = new ArrayList<User>();
		User user = null;
		for (int i = 0; i < 13; i++) {
			user = new User();
			user.setAge(1);
			list.add(user);
		}
		int listSize = list.size();
		int sheetSize = listSize % size == 0 ? listSize / size
				: ((listSize / size) + 1);
		System.out.println(sheetSize);

		int begin = 0;
		int end = 0;
		List<List<User>> sheetList = new ArrayList<List<User>>();
		for (int i = 0; i < sheetSize; i++) {
			end += size;

			if (end > listSize) {
				end = listSize;
			}
			sheetList.add(list.subList(begin, end));
			System.out.println(begin + " : " + end);
			begin = end;

		}

		System.out.println(sheetList);

	}

	/**
	 * 将list 集合切分成指定的份数，份数中的元素个数不一定
	 */
	@Test
	public void testSubList2() {

		int size = 4;
		List<User> list = new ArrayList<User>();
		User user = null;
		for (int i = 0; i < 5; i++) {
			user = new User();
			user.setAge(1);
			list.add(user);
		}
		int listSize = list.size();
		int sheetSize = listSize % size == 0 ? listSize / size
				: ((listSize / size) + 1);

		int begin = 0;
		int end = 0;
		List<List<User>> sheetList = new ArrayList<List<User>>(size);
		for (int i = 0; i < size; i++) {
			end += sheetSize;

			if (end > listSize) {
				end = listSize;
			}
			sheetList.add(list.subList(begin, end));
			System.out.println(begin + " : " + end);
			begin = end;

		}

		System.out.println(sheetList);

	}

	/**
	 * 将 list 集合平均切成指定的 份数，保证其中大部分的中的元素是相同的，得到最大的平均
	 */
	@Test
	public void testAvgList() {

		int size = 4;
		List<User> list = new ArrayList<User>();
		User user = null;
		for (int i = 0; i < 5; i++) {
			user = new User();
			user.setAge(1);
			list.add(user);
		}
		int listSize = list.size();

		List<List<User>> sheetList = new ArrayList<List<User>>(size);
		for(int i=0;i<size;i++){
			sheetList.add(new ArrayList<User>());
		}
		int index = 0;
		List<User> sheetInnerList = null;
		for (int i = 0; i < listSize; i++) {
			user = list.get(i);
			index =i%size;
			sheetInnerList= sheetList.get(index);
			sheetInnerList.add(user);
		}

		System.out.println(sheetList);
	}
}
