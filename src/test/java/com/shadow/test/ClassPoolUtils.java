package com.shadow.test;

import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.List;

import javassist.CannotCompileException;
import javassist.ClassClassPath;
import javassist.ClassPool;
import javassist.CtClass;
import javassist.CtField;
import javassist.CtMethod;
import javassist.NotFoundException;
import javassist.bytecode.AnnotationsAttribute;
import javassist.bytecode.ClassFile;
import javassist.bytecode.ConstPool;
import javassist.bytecode.FieldInfo;
import javassist.bytecode.MethodInfo;
import javassist.bytecode.annotation.Annotation;
import javassist.bytecode.annotation.BooleanMemberValue;
import javassist.bytecode.annotation.ByteMemberValue;
import javassist.bytecode.annotation.CharMemberValue;
import javassist.bytecode.annotation.DoubleMemberValue;
import javassist.bytecode.annotation.FloatMemberValue;
import javassist.bytecode.annotation.IntegerMemberValue;
import javassist.bytecode.annotation.LongMemberValue;
import javassist.bytecode.annotation.ShortMemberValue;
import javassist.bytecode.annotation.StringMemberValue;

import org.junit.Test;

import com.shadow.annotation.ExcelAnnotation;
import com.shadow.classloader.TestEntityClassLoader;
import com.shadow.model.User;

public class ClassPoolUtils {

	public static Class<?> aa(String entityClassName, String tableName) {

		Class<?> c = null;
		TestEntityClassLoader loader = new TestEntityClassLoader(
				ClassPoolUtils.class.getClassLoader());
		try {
			ClassPool classPool = ClassPool.getDefault();
			classPool.appendClassPath(new ClassClassPath(User.class));
			classPool.importPackage("com.shadow.annotation");
			CtClass clazz = classPool.get(entityClassName);

			ClassFile classFile = clazz.getClassFile();

			System.out.println("增强前Entity01:"
					+ clazz.getAnnotation(ExcelAnnotation.class));
			// System.out.println("增强前Table02:" +
			// clazz.getAnnotation(Table.class));

			ConstPool constPool = classFile.getConstPool();
			Annotation tableAnnotation = new Annotation(
					"com.shadow.annotation.ExcelAnnotation", constPool);
			tableAnnotation.addMemberValue("fieldName", new StringMemberValue(
					tableName, constPool));

			CtField[] declaredFields = clazz.getDeclaredFields();
			List<Annotation> AnnotationList = new ArrayList<Annotation>();
			for (CtField ctField : declaredFields) {
				ExcelAnnotation annotation = (ExcelAnnotation) ctField
						.getAnnotation(ExcelAnnotation.class);
				if (null == annotation) {
					continue;
				}
				Class<? extends ExcelAnnotation> class1 = annotation.getClass();
				if (null == class1) {
					continue;
				}
				Annotation tableAnnotation1 = new Annotation(
						"com.shadow.annotation.ExcelAnnotation", constPool);
				Method[] declaredMethods = class1.getDeclaredMethods();
				declaredMethods = class1.getMethods();
				String methodName = null;
				for (Method method : declaredMethods) {
					methodName = method.getName();
					System.out.println(methodName);
					if ("equals".equals(methodName)) {
						continue;
					} else if ("toString".equals(methodName)) {
						continue;
					} else if ("hashCode".equals(methodName)) {
						continue;
					} else if ("isProxyClass".equals(methodName)) {
						continue;
					} else if ("getInvocationHandler".equals(methodName)) {
						continue;
					} else if ("getProxyClass".equals(methodName)) {
						continue;
					} else if ("newProxyInstance".equals(methodName)) {
						continue;
					} else if ("wait".equals(methodName)) {
						continue;
					} else if ("getClass".equals(methodName)) {
						continue;
					} else if ("notify".equals(methodName)) {
						continue;
					} else if ("notifyAll".equals(methodName)) {
						continue;
					}

					Class<?> returnType = method.getReturnType();
					String name = returnType.getName();

					Object invoke = method.invoke(annotation, null);
					String invokeStr = String.valueOf(invoke);

					if ("fieldName".equals(methodName)) {
						if ("##default".equals(invokeStr)) {

							invokeStr = ctField.getName();
							;
						}

					}

					if ("short".equals(name) || "java.lang.Short".equals(name)) {
						short valueOf = Short.valueOf(invokeStr);
						tableAnnotation1.addMemberValue(methodName,
								new ShortMemberValue(valueOf, constPool));
					} else if ("int".equals(name)
							|| "java.lang.Integer".equals(name)) {

						int valueOf = Integer.valueOf(invokeStr);
						tableAnnotation1.addMemberValue(methodName,
								new IntegerMemberValue(valueOf, constPool));

					} else if ("long".equals(name)
							|| "java.lang.Long".equals(name)) {

						long valueOf = Long.valueOf(invokeStr);
						tableAnnotation1.addMemberValue(methodName,
								new LongMemberValue(valueOf, constPool));

					} else if ("float".equals(name)
							|| "java.lang.Float".equals(name)) {

						float valueOf = Float.valueOf(invokeStr);
						tableAnnotation1.addMemberValue(methodName,
								new FloatMemberValue(valueOf, constPool));
					} else if ("double".equals(name)
							|| "java.lang.Double".equals(name)) {

						double valueOf = Double.valueOf(invokeStr);
						tableAnnotation1.addMemberValue(methodName,
								new DoubleMemberValue(valueOf, constPool));
					} else if ("char".equals(name)
							|| "java.lang.Character".equals(name)) {

						char invokChar = (char) invoke;
						tableAnnotation1.addMemberValue(methodName,
								new CharMemberValue(invokChar, constPool));

					} else if ("byte".equals(name)
							|| "java.lang.Byte".equals(name)) {

						byte valueOf = Byte.valueOf(invokeStr);
						tableAnnotation1.addMemberValue(methodName,
								new ByteMemberValue(valueOf, constPool));
					} else if ("java.lang.String".equals(name)) {

						tableAnnotation1.addMemberValue(methodName,
								new StringMemberValue(invokeStr, constPool));

					} else if ("boolean".equals(name)
							|| "java.lang.Boolean".equals(name)) {

						boolean valueOf = Boolean.valueOf(invokeStr);
						tableAnnotation1.addMemberValue(methodName,
								new BooleanMemberValue(valueOf, constPool));
					}

				}

				String typeName = tableAnnotation1.getTypeName();

				System.out.println(typeName);
				AnnotationList.add(tableAnnotation1);
			}

			List<ExcelAnnotation> excelAnnotation = new ArrayList<ExcelAnnotation>();
			for (Annotation annotation : AnnotationList) {
				excelAnnotation.add((ExcelAnnotation) annotation);
			}

			for (ExcelAnnotation excelAnnotation2 : excelAnnotation) {
				System.out.println(excelAnnotation2.fieldName());
				System.out.println(excelAnnotation2.orderNum());
				System.out.println(excelAnnotation2.rowHeader());
				System.out.println(excelAnnotation2.dateFormat());
			}

			// 获取运行时注解属性
			AnnotationsAttribute attribute = (AnnotationsAttribute) classFile
					.getAttribute(AnnotationsAttribute.visibleTag);
			attribute.addAnnotation(tableAnnotation);
			classFile.addAttribute(attribute);
			classFile.setVersionToJava5();
			// clazz.writeFile();

			System.out.println("增强后Entity001:"
					+ clazz.getAnnotation(ExcelAnnotation.class));
			// System.out.println("增强后Table002:" +
			// clazz.getAnnotation(Table.class));
			// TODO 当前ClassLoader中必须尚未加载该实体。（同一个ClassLoader加载同一个类只会加载一次）
			c = clazz.toClass();
			System.out.println("增强后toClass-Entity0001:"
					+ c.getAnnotation(ExcelAnnotation.class));
			// System.out.println("增强后toClass-Table0002:" +
			// c.getAnnotation(Table.class));
		} catch (Exception e) {
			e.printStackTrace();
		}

		return c;

	}

	public static void main(String[] args) throws InstantiationException,
			IllegalAccessException, NotFoundException {

		System.out.println(User.class.getName());

	}

	@Test
	public void update() throws NotFoundException, CannotCompileException {
		ClassPool pool = ClassPool.getDefault();
		// 获取要修改的类的所有信息
		CtClass ct = pool.get("com.shadow.model.User");
		// 获取类里的name属性
		CtField cf = ct.getField("name");
		// 获取属性信息
		FieldInfo fieldInfo = cf.getFieldInfo();
		ConstPool cp = fieldInfo.getConstPool();
		System.out.println("属性名称===" + cf.getName());

		// 获取注解属性
		AnnotationsAttribute attribute = (AnnotationsAttribute) fieldInfo
				.getAttribute(AnnotationsAttribute.visibleTag);
		System.out.println(attribute);
		// 获取注解
		Annotation annotation = attribute
				.getAnnotation("com.shadow.annotation.ExcelAnnotation");
		System.out.println(annotation);
		// 获取注解的值
		StringMemberValue memberValue = (StringMemberValue) annotation
				.getMemberValue("fieldName");
		if (null == memberValue) {
			// 修改注解
			annotation.addMemberValue("fieldName", new StringMemberValue(
					"basic-entity", cp));
			attribute.setAnnotation(annotation);
			fieldInfo.addAttribute(attribute);
		}
		String text = null;
		System.out.println("注解名称===" + text);

		// 获取注解的值
		System.out.println("修改后");
		System.out.println(annotation);
		// text = ((StringMemberValue) memberValue).getValue();
		// System.out.println("修改后的注解属性值===" + text);

		Class clazz = null;
		TestEntityClassLoader loader = new TestEntityClassLoader(
				ClassPoolUtils.class.getClassLoader());
		clazz = ct.toClass(loader, null);

		System.out.println(clazz);
		Field[] fields = clazz.getDeclaredFields();
		for (Field field : fields) {
			ExcelAnnotation annotation4 = field
					.getAnnotation(ExcelAnnotation.class);
			System.out.println(annotation4);
		}

	}

	@Test
	public void update01() throws NotFoundException {
		ClassPool pool = ClassPool.getDefault();
		// 获取需要修改的类
		CtClass ct = pool.get("com.shadow.model.User");

		// 获取类里的所有方法
		CtMethod[] cms = ct.getDeclaredMethods();
		CtMethod cm = cms[0];
		System.out.println("方法名称====" + cm.getName());

		MethodInfo minInfo = cm.getMethodInfo();
		// 获取类里的em属性
		CtField cf = ct.getField("name");
		FieldInfo fieldInfo = cf.getFieldInfo();

		System.out.println("属性名称===" + cf.getName());

		ConstPool cp = fieldInfo.getConstPool();
		// 获取注解信息
		AnnotationsAttribute attribute2 = new AnnotationsAttribute(cp,
				AnnotationsAttribute.visibleTag);
		Annotation annotation = new Annotation(
				"com.shadow.annotation.ExcelAnnotation", cp);

		// 修改名称为unitName的注解
		annotation.addMemberValue("fieldName", new StringMemberValue(
				"basic-entity", cp));
		attribute2.setAnnotation(annotation);
		minInfo.addAttribute(attribute2);

		// 打印修改后方法
		Annotation annotation2 = attribute2
				.getAnnotation("com.shadow.annotation.ExcelAnnotation");
		String text = ((StringMemberValue) annotation2
				.getMemberValue("fieldName")).getValue();

		System.out.println("修改后的注解名称===" + text);
	}

	@Test
	public void update2() throws NotFoundException, CannotCompileException {
		ClassPool pool = ClassPool.getDefault();
		// 获取需要修改的类
		CtClass ct = pool.get("com.shadow.model.User");

		// 获取类里的所有方法
		CtMethod[] cms = ct.getDeclaredMethods();
		CtMethod cm = cms[0];
		System.out.println("方法名称====" + cm.getName());

		MethodInfo minInfo = cm.getMethodInfo();
		// 获取类里的em属性
		CtField cf = ct.getField("name");

		FieldInfo fieldInfo = cf.getFieldInfo();

		System.out.println("属性名称===" + cf.getName());

		ConstPool cp = fieldInfo.getConstPool();
		// 获取注解信息
		AnnotationsAttribute attribute2 = new AnnotationsAttribute(cp,
				AnnotationsAttribute.visibleTag);
		Annotation annotation = new Annotation(
				"com.shadow.annotation.ExcelAnnotation", cp);

		Annotation annotation2 = attribute2
				.getAnnotation("com.shadow.annotation.ExcelAnnotation");
		String text = ((StringMemberValue) annotation2
				.getMemberValue("fieldName")).getValue();

		System.out.println("修改前的注解名称===" + text);
		// 修改名称为unitName的注解
		annotation.addMemberValue("fieldName", new StringMemberValue(
				"basic-entity", cp));
		attribute2.setAnnotation(annotation);
		minInfo.addAttribute(attribute2);

		// 打印修改后方法
		Annotation annotation3 = attribute2
				.getAnnotation("com.shadow.annotation.ExcelAnnotation");
		String text3 = ((StringMemberValue) annotation2
				.getMemberValue("fieldName")).getValue();

		System.out.println("修改后的注解名称===" + text3);

		Class clazz = null;
		TestEntityClassLoader loader = new TestEntityClassLoader(
				ClassPoolUtils.class.getClassLoader());
		clazz = ct.toClass(loader, null);

		System.out.println(clazz);
		Field[] fields = clazz.getDeclaredFields();
		for (Field field : fields) {
			ExcelAnnotation annotation4 = field
					.getAnnotation(ExcelAnnotation.class);
			System.out.println(annotation4);
		}

	}
}
