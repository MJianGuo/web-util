package com.shadow.callable;

import java.io.File;
import java.io.FileOutputStream;
import java.io.OutputStream;
import java.util.Calendar;
import java.util.Date;
import java.util.concurrent.Callable;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;

import org.apache.poi.hssf.usermodel.HSSFCell;
import org.apache.poi.hssf.usermodel.HSSFCellStyle;
import org.apache.poi.hssf.usermodel.HSSFRichTextString;
import org.apache.poi.hssf.usermodel.HSSFRow;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.hssf.util.HSSFColor;
import org.apache.poi.ss.usermodel.CellStyle;
import org.apache.poi.ss.usermodel.RichTextString;

public class ExcellCallable implements Callable<HSSFWorkbook> {

	private CellStyle cellStyle;

	@Override
	public HSSFWorkbook call() throws Exception {

		String filePath = "F:/test.xls";
		File file = new File(filePath);
		OutputStream fileOutputStream = new FileOutputStream(file);

		// 1.创建一个代表 excel 文件的对象
		HSSFWorkbook hSSFWorkbook = new HSSFWorkbook();

		// 2.创建一个sheet 工作表单
		HSSFSheet hSSFSheet = hSSFWorkbook.createSheet("第一个sheet");
		// 3获取sheet 表单的第一行
		HSSFRow hSSFRow = hSSFSheet.createRow(0);

		// 样式
		CellStyle cellStyle = hSSFWorkbook.createCellStyle();
		// 左右上下边框样式
		cellStyle.setBorderLeft(HSSFCellStyle.BORDER_THIN);
		cellStyle.setBorderRight(HSSFCellStyle.BORDER_THIN);
		cellStyle.setBorderTop(HSSFCellStyle.BORDER_THIN);
		cellStyle.setBorderBottom(HSSFCellStyle.BORDER_THIN);

		// 左右上下边框颜色(深蓝色),只有设置了上下左右边框以后给边框设置颜色才会生效
		cellStyle.setLeftBorderColor(HSSFColor.BLACK.index);
		cellStyle.setRightBorderColor(HSSFColor.BLACK.index);
		cellStyle.setTopBorderColor(HSSFColor.BLACK.index);
		cellStyle.setBottomBorderColor(HSSFColor.BLACK.index);

		hSSFRow.setRowStyle(cellStyle);

		// 4.获取第一行中第一个单元格
		HSSFCell hSSFCel = hSSFRow.createCell(0); // (0x0)
		// 5. 设置单元格的内容
		hSSFCel.setCellValue(true);
		hSSFCel.setCellStyle(cellStyle);

		// 重复 4 。5
		hSSFCel = hSSFRow.createCell(1);
		hSSFCel.setCellValue(Calendar.getInstance());

		hSSFCel = hSSFRow.createCell(2);
		hSSFCel.setCellValue(new Date());
		hSSFCel = hSSFRow.createCell(3);
		double dd = 2.515D;
		hSSFCel.setCellValue(dd);
		hSSFCel = hSSFRow.createCell(4);
		RichTextString richTextString = new HSSFRichTextString(
				"这是一个特殊格式的 RichTextString 字符串");
		hSSFCel.setCellValue(richTextString);
		hSSFCel = hSSFRow.createCell(5);
		hSSFCel.setCellValue("这是字符串");

		hSSFWorkbook.write(fileOutputStream);

		return hSSFWorkbook;
	}

	public static void main(String[] args) throws InterruptedException,
			ExecutionException {

		ExecutorService excutorService = Executors.newFixedThreadPool(1);

		Future<HSSFWorkbook> submit = excutorService
				.submit(new ExcellCallable());

		HSSFWorkbook hssfWorkbook = submit.get();
		System.out.println(hssfWorkbook);

		excutorService.shutdown();
		System.out.println("==================");
	}

}
