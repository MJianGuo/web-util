package com.shadow.model;

import com.shadow.annotation.ExcelAnnotation;

public class User {

	@ExcelAnnotation(orderNum = 1, rowHeader = "姓名")
	private String name;
	@ExcelAnnotation(orderNum = 2, rowHeader = "年龄")
	private int age;

	private short _short;
	private int _int;
	private long _long;
	private boolean _boolean;
	private float _float;
	private double _double;
	private char _char;
	private byte _byte;
	private String _string;
	private User _user;
	
	private final int _final_int=0;
	public final short _final_short=1;
	public static int _static_int =2;
	private static short _static_short=9;
	public static final int _static_final_int=23;
	int _int_int =9;
	

	public User() {
	}

	public User(String name, int age) {
		super();
		this.name = name;
		this.age = age;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public int getAge() {
		return age;
	}

	public void setAge(int age) {
		this.age = age;
	}

	public short get_short() {
		return _short;
	}

	public void set_short(short _short) {
		this._short = _short;
	}

	public int get_int() {
		return _int;
	}

	public void set_int(int _int) {
		this._int = _int;
	}

	public long get_long() {
		return _long;
	}

	public void set_long(long _long) {
		this._long = _long;
	}

	public boolean is_boolean() {
		return _boolean;
	}

	public void set_boolean(boolean _boolean) {
		this._boolean = _boolean;
	}

	public float get_float() {
		return _float;
	}

	public void set_float(float _float) {
		this._float = _float;
	}

	public double get_double() {
		return _double;
	}

	public void set_double(double _double) {
		this._double = _double;
	}

	public char get_char() {
		return _char;
	}

	public void set_char(char _char) {
		this._char = _char;
	}

	public byte get_byte() {
		return _byte;
	}

	public void set_byte(byte _byte) {
		this._byte = _byte;
	}

	public String get_string() {
		return _string;
	}

	public void set_string(String _string) {
		this._string = _string;
	}

	public User get_user() {
		return _user;
	}

	public void set_user(User _user) {
		this._user = _user;
	}

}
