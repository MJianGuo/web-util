package com.shadow.model;

import java.util.Date;

import com.shadow.annotation.ExcelAnnotation;

/**
 * 本类是配合 ExcelUtil 使用的类，用于将本类中的数据生成
 * 
 * @author MJG
 * 
 */
public class StudentExcel {

	@ExcelAnnotation(orderNum = 2, rowHeader = "姓名")
	private String name;
	@ExcelAnnotation(orderNum = 3, rowHeader = "年龄")
	private int age;
	@ExcelAnnotation(orderNum = 1, rowHeader = "生日")
	private Date birthday;

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public int getAge() {
		return age;
	}

	public void setAge(int age) {
		this.age = age;
	}

	public Date getBirthday() {
		return birthday;
	}

	public void setBirthday(Date birthday) {
		this.birthday = birthday;
	}

	@Override
	public String toString() {
		return "StudentExcel [name=" + name + ", age=" + age + ", birthday="
				+ birthday + "]";
	}

}
