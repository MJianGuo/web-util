package com.shadow.example;

import java.io.ByteArrayOutputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.lang.reflect.Field;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.apache.poi.hssf.usermodel.HSSFCell;
import org.apache.poi.hssf.usermodel.HSSFCellStyle;
import org.apache.poi.hssf.usermodel.HSSFRow;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.CellStyle;
import org.junit.Test;

import com.shadow.annotation.ExcelAnnotation;
import com.shadow.enumation.DateEnumation;
import com.shadow.model.StudentExcel;
import com.shadow.util.DateUtil;
import com.shadow.util.ExcelUtil;

/**
 * 本类是给予使用 ExcelUtil 的例子
 * 
 * @author MJG
 * 
 */
public class ExcelUtilExample {

	/**
	 * 测试生成excel
	 */
	@Test
	public void createExcel() {

		StudentExcel student = null;
		List<StudentExcel> studentList = new ArrayList<StudentExcel>();
		for (int i = 0; i < 10; i++) {
			student = new StudentExcel();
			student.setName("小明 --> " + i);
			student.setAge(i);
			student.setBirthday(new Date());
			studentList.add(student);
		}
		String dirPath = "E:/student";
		String excelName = "学生表.xls";

		String excelFilePath = ExcelUtil.createExcel(studentList, dirPath, excelName);
		System.out.println(excelFilePath);
	}

	/**
	 * 测试读取excel
	 */
	@Test
	public void readExcel() {
		String excelFilePath = "E:/student/学生表.xls";
		List<StudentExcel> studentList = ExcelUtil.readExcelToBean(StudentExcel.class, excelFilePath);
		for (StudentExcel studentExcel : studentList) {
			System.out.println(studentExcel);
		}
	}

	@Test
	public void test00()
			throws NoSuchFieldException, SecurityException, IllegalArgumentException, IllegalAccessException {
		StudentExcel student = new StudentExcel();
		student.setName("小明 --> ");
		student.setAge(20);
		student.setBirthday(new Date());

		Class<? extends StudentExcel> clazz = student.getClass();

		Field field = clazz.getDeclaredField("name");
		field.setAccessible(true);
		Object object = field.get(student);
		Class<? extends Field> fieldClazz = field.getClass();
		Class<?> type = field.getType();

		System.out.println(field);
		System.out.println(fieldClazz);
		System.out.println(type);
		System.out.println(type.getName());
		System.out.println(object);

		String[] rowHeaderArr = new String[] { "姓名", "年龄", "生日" };
		String[] filedNameArr = new String[] { "name", "age", "birthday" };
	}

	@Test
	public void test01() throws IOException {

		// 创建HSSFWorkbook对象
		HSSFWorkbook wb = new HSSFWorkbook();
		// 创建HSSFSheet对象
		HSSFSheet sheet = wb.createSheet("sheet0");
		// 创建HSSFRow对象
		HSSFRow row = sheet.createRow(0);
		// 创建HSSFCell对象
		HSSFCell cell = row.createCell(0);
		// 设置单元格的值
		cell.setCellValue("单元格中的中文");
		// 输出Excel文件
		FileOutputStream output = new FileOutputStream("d:\\workbook.xls");
		wb.write(output);
		output.flush();

	}

	
	public static <T> ByteArrayOutputStream createExcelToMemory(List<T> excelDataList) {

		String[] headerRowNameArr = new String[] { "姓名", "年龄", "生日" };
		String[] filedNameArr = new String[] { "name", "age", "birthday" };

		// 创建HSSFWorkbook对象
		HSSFWorkbook hssfWorkbook = new HSSFWorkbook();
		HSSFSheet hssfSheet = createHeaderRow(hssfWorkbook, "", headerRowNameArr);

		int rowNum;
		if (null == headerRowNameArr || headerRowNameArr.length == 0) {
			rowNum = 0;
		} else {
			rowNum = 1;
		}
		T t;
		HSSFRow hssFRow;
		for (int i = 0, size = excelDataList.size(); i < size; i++) {
			t = excelDataList.get(i);
			hssFRow = hssfSheet.createRow(rowNum + i);
			setRowCellContent(hssFRow, t, filedNameArr);
		}

		ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
		try {
			hssfWorkbook.write(byteArrayOutputStream);
			byteArrayOutputStream.flush();
		} catch (IOException e) {
			e.printStackTrace();
		}
		return byteArrayOutputStream;
	}

	private static <T> HSSFRow setRowCellContent(HSSFRow hssFRow, T t, String[] filedNameArr) {

		HSSFCell hssfCell;
		String fieldName;
		for (int i = 0, length = filedNameArr.length; i < length; i++) {
			hssfCell = hssFRow.createCell(i);
			fieldName = filedNameArr[i];
			setCellValue(hssfCell, t, fieldName);
		}
		return hssFRow;
	}

	private static <T> HSSFCell setCellValue(HSSFCell hssfCell, T t, String fieldName) {

		Object[] filedTypeAndValue = getFiledTypeAndValue(t, fieldName);
		String fieldTypeName = filedTypeAndValue[0].toString();
		Object fieldValue = filedTypeAndValue[1];

		String fieldValueStr = "";
		if (null == fieldValue|| fieldValue.toString().trim().equals("")){
			hssfCell.setCellType(HSSFCell.CELL_TYPE_STRING);
			hssfCell.setCellValue(fieldValueStr);
		}else if("int".equals(fieldTypeName) || "java.lang.Integer".equals(fieldTypeName)) {

			fieldValueStr = String.valueOf(fieldValue);
			hssfCell.setCellType(HSSFCell.CELL_TYPE_NUMERIC);
			hssfCell.setCellValue(Integer.parseInt(fieldValueStr));
		} else if ("double".equals(fieldTypeName) || "java.lang.Double".equals(fieldTypeName)) {

			fieldValueStr = String.valueOf(fieldValue);
			hssfCell.setCellType(HSSFCell.CELL_TYPE_NUMERIC);
			hssfCell.setCellValue(Double.parseDouble(fieldValueStr));
		} else if ("boolean".equals(fieldTypeName) || "java.lang.Boolean".equals(fieldTypeName)) {

			fieldValueStr = String.valueOf(fieldValue);
			hssfCell.setCellType(HSSFCell.CELL_TYPE_BOOLEAN);
			hssfCell.setCellValue(Boolean.parseBoolean(fieldValueStr));
		} else if ("java.lang.String".equals(fieldTypeName)) {

			fieldValueStr = String.valueOf(fieldValue);
			hssfCell.setCellType(HSSFCell.CELL_TYPE_STRING);
			hssfCell.setCellValue(fieldValueStr);
		} else if ("java.util.Date".equals(fieldTypeName)) {
			fieldValueStr = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(fieldValue);
			hssfCell.setCellType(HSSFCell.CELL_TYPE_STRING);
			hssfCell.setCellValue(fieldValueStr);
		} else if (fieldName.toLowerCase().endsWith("time")) {
			fieldValueStr = String.valueOf(fieldValue);
			long timeStamp = Long.parseLong(fieldValueStr);
			fieldValueStr = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(new Date(timeStamp));
			hssfCell.setCellType(HSSFCell.CELL_TYPE_STRING);
			hssfCell.setCellValue(fieldValueStr);
		} else {
			fieldValueStr = String.valueOf(fieldValueStr);
			hssfCell.setCellType(HSSFCell.CELL_TYPE_STRING);
			hssfCell.setCellValue(fieldValueStr);
		}

		return hssfCell;

	}

	private static <T> Object[] getFiledTypeAndValue(T t, String fieldName) {

		Object fieldValue = null;
		String fieldTypeName = null;
		try {
			Class<? extends Object> clazz = t.getClass();
			Field field = clazz.getDeclaredField(fieldName);
			field.setAccessible(true);
			fieldValue = field.get(t);
			fieldTypeName = field.getType().getName();
		} catch (NoSuchFieldException e) {
			e.printStackTrace();
		} catch (SecurityException e) {
			e.printStackTrace();
		} catch (IllegalArgumentException e) {
			e.printStackTrace();
		} catch (IllegalAccessException e) {
			e.printStackTrace();
		}

		return new Object[] { fieldTypeName, fieldValue };
	}

	public static HSSFSheet createHeaderRow(HSSFWorkbook hssfWorkbook, String sheetName, String[] headerRowNameArr) {

		HSSFSheet hssfSheet;
		if (null == sheetName || sheetName.trim().equals("")) {
			hssfSheet = hssfWorkbook.createSheet();
		} else {
			hssfSheet = hssfWorkbook.createSheet(sheetName);
		}

		if (null != headerRowNameArr && headerRowNameArr.length != 0) {
			HSSFRow headerRow = hssfSheet.createRow(0);
			String headerRowName;
			HSSFCell headerRowCell;
			for (int i = 0, length = headerRowNameArr.length; i < length; i++) {
				headerRowName = headerRowNameArr[i];
				headerRowCell = headerRow.createCell(i);
				headerRowCell.setCellValue(headerRowName);
			}
		}

		return hssfSheet;
	}
}
