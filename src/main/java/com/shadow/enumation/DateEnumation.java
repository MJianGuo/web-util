package com.shadow.enumation;

/**
 * 这是关于时间以及时间格式的枚举类，配合 DateUtil 工具类进行使用
 * 
 * @author MJG
 * 
 */
public enum DateEnumation {

	/**
	 * 默认的时间格式：yyyy-MM-dd HH:mm:ss
	 */
	DEFAULT_FORMAT("yyyy-MM-dd HH:mm:ss"),
	/**
	 * 时间格式：yyyyMMddHHmmss
	 */
	YYYYMMDDHHMMSS_FORMAT("yyyyMMddHHmmss"),
	/**
	 * 天的起始时间格式：yyyy-MM-dd 00:00:00
	 */
	START_DAY_FORAMT("yyyy-MM-dd 00:00:00"),
	/**
	 * 天的结束时间格式：yyyy-MM-dd 23:59:59
	 */
	END_DAY_FORMAT("yyyy-MM-dd 23:59:59");

	private String format;

	DateEnumation(String format) {

		this.format = format;
	}

	public String getFormat() {
		return format;
	}

	public void setFormat(String format) {
		this.format = format;
	}

}
