package com.shadow.enumation;

/**
 * 与周相关的枚举类
 * 
 * 
 */
public enum WeekEnumation {

	MONDAY(2,"星期一"),
	TUESDAY(3,"星期二"),
	WEDNESDAY(4,"星期三"),
	THURSDAY(5,"星期四"),
	FRIDAY(6,"星期五"),
	SATURDAY (7,"星期六"),
	SUNDAY(1,"星期日");
	
	/**
	 * 根据index 获取具体的周几
	 * 
	 * @param index
	 * @return
	 */
	public static String week(int index){
		
		for (WeekEnumation weekEnumation : WeekEnumation.values()) {
			if(index == weekEnumation.getIndex()){
				return weekEnumation.getWeek();
			}
		}

		return null;
	}
	
	private int index;
	private String week;
	WeekEnumation(int index,String week){
	
		this.index = index;
		this.week = week;
	}
	
	public int getIndex() {
		return index;
	}
	public void setIndex(int index) {
		this.index = index;
	}
	public String getWeek() {
		return week;
	}
	public void setWeek(String week) {
		this.week = week;
	}
	

	
}
