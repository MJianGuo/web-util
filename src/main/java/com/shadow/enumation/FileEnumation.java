package com.shadow.enumation;

import com.shadow.util.OperaSystemEnvUtil;

/**
 * 这是与文件操作相关的枚举类与 FileUtil 工具类进行配合使用
 * 
 * @author MJG
 * 
 */
public enum FileEnumation {

	/**
	 * 行分隔符
	 */
	LINE_SEPARATOR(OperaSystemEnvUtil.getLineSeparator()),
	/**
	 * 文件路径分隔符
	 */
	FILE_PATH_SEPARATOR(OperaSystemEnvUtil.getFileSpearator());

	private String value;

	private FileEnumation(String value) {
		this.value = value;
	}

	public String getValue() {
		return value;
	}

	public void setValue(String value) {
		this.value = value;
	}

}
