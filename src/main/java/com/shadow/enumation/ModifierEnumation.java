package com.shadow.enumation;

/**
 * 该枚举是与描述类或者方法以及字段的修改付和数字标识对应的枚举类
 * 
 * @author MJG
 *
 */
public enum ModifierEnumation {

	DEFAULT("", 0),
	PRIVATE("private",2),
	PROTECTED("protected",4),
	PUBLIC("public",1),
	STATIC("static",8),
	PRIVATE_STATIC("private static",10),
	PROTECTED_STATIC("protected static",12),
	PUBLIC_STATIC("public static",9),
	FINAL("final",16),
	PRIVATE_FINAL("private final",18),
	PROTECTED_FINAL("protected final",20),
	PUBLIC_FINAL("public final",17),
	STATIC_FINAL("static final",24),
	PRIVATE_STATIC_FINAL("private static final",26),
	PROTECTED_STATIC_FINAL("protected static final",28),
	PUBLIC_STATIC_FINAL("public static final",25);

	private String modifierStr;
	private int modifierNum;

	ModifierEnumation(String modifierStr, int modifierNum) {
		this.modifierStr = modifierStr;
		this.modifierNum = modifierNum;
	}

	/**
	 * 根据修饰符的数字表现形式获得其真实的修饰符的字符串表现形式
	 * 
	 * @param modifierNum
	 *            修饰符的数字表现形式，参见 java.lang.reflect.Modifier
	 * @return 修饰符的字符串表现形式：例如；public 、private 等
	 */
	public static String getModifier(int modifierNum) {

		for (ModifierEnumation mofifier : ModifierEnumation.values()) {
			if (mofifier.getModifierNum() == modifierNum) {
				return mofifier.getModifierStr();
			}
		}

		return null;

	}

	public String getModifierStr() {
		return modifierStr;
	}

	public void setModifierStr(String modifierStr) {
		this.modifierStr = modifierStr;
	}

	public int getModifierNum() {
		return modifierNum;
	}

	public void setModifierNum(int modifierNum) {
		this.modifierNum = modifierNum;
	}

}
