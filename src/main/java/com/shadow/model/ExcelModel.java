package com.shadow.model;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang.StringUtils;

/**
 * 用于描述 excel 文件信息的类
 * 
 * @author MJG
 * 
 */
public class ExcelModel<T> {

	/**
	 * 该 excel 中包含的 sheet 工作表单
	 */
	private List<ExcelSheetModel<T>> sheetList;
	/**
	 * 该 excel 文件的名字
	 */
	private String excelName;
	/**
	 * 该 excel 文件存储的文件夹路径
	 */
	private String dirPath;

	public ExcelModel() {
		sheetList = new ArrayList<ExcelSheetModel<T>>();
	}

	public ExcelModel(int sheetListSize, String dirPath, String excelName) {
		sheetList = new ArrayList<ExcelSheetModel<T>>(sheetListSize);
		this.dirPath = dirPath;
		if (StringUtils.isNotBlank(excelName) && !excelName.endsWith(".xls")) {
			excelName += ".xls";
		}
		this.excelName = excelName;
	}

	public ExcelModel(int sheetListSize) {
		sheetList = new ArrayList<ExcelSheetModel<T>>(sheetListSize);
	}

	/**
	 * 向 ExcelModel 中添加工作表单
	 * 
	 * @param excelSheetModel
	 */
	public void addSheet(ExcelSheetModel<T> excelSheetModel) {
		sheetList.add(excelSheetModel);
	}

	public List<ExcelSheetModel<T>> getSheetList() {
		return sheetList;
	}

	public String getExcelName() {
		return excelName;
	}

	public void setExcelName(String excelName) {
		this.excelName = excelName;
	}

	public String getDirPath() {
		return dirPath;
	}

	public void setDirPath(String dirPath) {
		this.dirPath = dirPath;
	}

}
