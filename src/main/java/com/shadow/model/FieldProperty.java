package com.shadow.model;

import java.lang.annotation.Annotation;
import java.lang.reflect.Type;

import org.junit.Test;

import com.shadow.enumation.ModifierEnumation;

/**
 * 该类是封装类中字段的所有的属性信息
 * 
 * @author MJG
 * 
 */
public class FieldProperty {

	/** 字段名称 */
	private String fieldName;
	/** 该字段的修饰符的数字表现形式，参见 java.lang.reflect.Modifier */
	private int modifier;
	/** 该字段的修饰符的字符串表现形式，参见 java.lang.reflect.Modifier */
	private String modifierName;
	/** 该字段的类型 */
	private Class<?> clazz;
	/** 该字段在那个类中进行声明的 */
	private Class<?> declaringClazz;
	/** 该字段的类型 */
	private Type type;
	/** 该字段在类中对应值 */
	private Object value;
	/** 该字段上的所有的注解 */
	private Annotation[] annotationArr;

	public String getFieldName() {
		return fieldName;
	}

	public void setFieldName(String fieldName) {
		this.fieldName = fieldName;
	}

	public int getModifier() {
		return modifier;
	}

	public void setModifier(int modifier) {
		setModifierName(ModifierEnumation.getModifier(modifier));
		this.modifier = modifier;
	}

	public String getModifierName() {
		return modifierName;
	}

	public void setModifierName(String modifierName) {
		this.modifierName = modifierName;
	}

	public Class<?> getClazz() {
		return clazz;
	}

	public void setClazz(Class<?> clazz) {
		this.clazz = clazz;
	}

	public Type getType() {
		return type;
	}

	public void setType(Type type) {
		this.type = type;
	}

	public Annotation[] getAnnotationArr() {
		return annotationArr;
	}

	public void setAnnotationArr(Annotation[] annotationArr) {
		this.annotationArr = annotationArr;
	}

	public Class<?> getDeclaringClazz() {
		return declaringClazz;
	}

	public void setDeclaringClazz(Class<?> declaringClazz) {
		this.declaringClazz = declaringClazz;
	}

	public Object getValue() {
		return value;
	}

	public void setValue(Object value) {
		this.value = value;
	}

	/**
	 * 获取该字段上指定类型 的注解
	 * 
	 * @param clazz
	 *            指定的注解类型
	 * @return
	 */
	@SuppressWarnings("unchecked")
	public <T> T getAnnotation(Class<? extends T> clazz) {

		int length = 0;
		if (null == annotationArr || ((length = annotationArr.length) == 0)) {
			return null;
		}
		Annotation annotation = null;
		String clazzName = clazz.getName();
		for (int i = 0; i < length; i++) {

			annotation = annotationArr[i];
			if ((annotation.annotationType().getName()).equals(clazzName)) {
				return (T) annotation;
			}
		}

		return null;
	}

	@Test
	public void test() {

		Object a = "aa";

		System.out.println(a.getClass().getName());
		System.out.println(a instanceof String);

	}
}
