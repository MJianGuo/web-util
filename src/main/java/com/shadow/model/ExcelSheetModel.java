package com.shadow.model;

import java.util.List;

/**
 * 用于描述 excel 中具体的某个工作 sheet 表单的类
 * 
 * @author MJG
 * 
 */
public class ExcelSheetModel<T> {

	/**
	 * 该 sheet 工作表单的名称
	 */
	private String sheetName;
	/**
	 * 该 sheet 工作表单中，表头的显示的中文名称
	 */
	private String[] rowHeaderNameArr;
	/**
	 * 该 sheet 工作表单中与表头中文名称对应的某张表中的字段名称
	 */
	private String[] rowHeaderPropertyArr;

	/**
	 * 该 sheet 工作表单对应的内容数据
	 */
	private List<T> sheetContentDataList;

	public ExcelSheetModel() {
	}

	public ExcelSheetModel(String[] rowHeaderNameArr,
			String[] rowHeaderPropertyArr, List<T> sheetContentDataList) {
		super();
		this.rowHeaderNameArr = rowHeaderNameArr;
		this.rowHeaderPropertyArr = rowHeaderPropertyArr;
		this.sheetContentDataList = sheetContentDataList;
	}

	public String getSheetName() {
		return sheetName;
	}

	public void setSheetName(String sheetName) {
		this.sheetName = sheetName;
	}

	public String[] getRowHeaderNameArr() {
		return rowHeaderNameArr;
	}

	public void setRowHeaderNameArr(String[] rowHeaderNameArr) {
		this.rowHeaderNameArr = rowHeaderNameArr;
	}

	public String[] getRowHeaderPropertyArr() {
		return rowHeaderPropertyArr;
	}

	public void setRowHeaderPropertyArr(String[] rowHeaderPropertyArr) {
		this.rowHeaderPropertyArr = rowHeaderPropertyArr;
	}

	public List<T> getSheetContentDataList() {
		return sheetContentDataList;
	}

	public void setSheetContentDataList(List<T> sheetContentDataList) {
		this.sheetContentDataList = sheetContentDataList;
	}

}
