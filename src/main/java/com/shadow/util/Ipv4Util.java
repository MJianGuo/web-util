package com.shadow.util;

import java.net.InetAddress;
import java.net.UnknownHostException;

import org.apache.log4j.Logger;

public final class Ipv4Util {

	private static Logger log = Logger.getLogger(Ipv4Util.class);

	private Ipv4Util() {
	}

	/**
	 * 获取本机的 IP
	 * 
	 * @return
	 */
	public static String getLocalIpv4() {

		InetAddress inetAddress = null;
		try {
			inetAddress = InetAddress.getLocalHost();
			return inetAddress.getHostAddress();
		} catch (UnknownHostException e) {
			log.error(e.getMessage(), e);
		}

		return null;
	}

}
