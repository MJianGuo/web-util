package com.shadow.util;

import java.io.File;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Reader;
import java.io.UnsupportedEncodingException;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;

/**
 * 项目系统相关的工具类
 * 
 * @author MJG
 * 
 */
public final class SystemUtil {

	private static Logger log = Logger.getLogger(SystemUtil.class);

	private SystemUtil() {
	}

	/**
	 * 获取系统运行的根目录
	 * <p>
	 * 注意：与 项目打成 jar 包运行时，获取jar 包外边的配置文件的路径有关
	 * 
	 * @return String
	 */
	public static String getSystemRootDir() {

		String systemDir = "";
		String pathSeparator = System.getProperty("path.separator");
		String fileSeparator = System.getProperty("file.separator");
		systemDir = System.getProperty("user.dir");
		// systemDir = System.getProperty("java.class.path");
		String[] temp = systemDir.split(pathSeparator);
		systemDir = temp[temp.length - 1];
		if (systemDir.endsWith(".jar")) {
			int len = systemDir.lastIndexOf(fileSeparator);
			systemDir = systemDir.substring(0, len + 1);
		}
		if (!systemDir.endsWith(fileSeparator)) {
			systemDir = systemDir + fileSeparator;
		}
		return systemDir;
	}

	/**
	 * 获取 web 项目运行的根目录
	 * 
	 * @return String
	 */
	public static String getWebRootDir() {
		return getWebInfDir().substring(0, getWebInfDir().indexOf("WEB-INF"));
	}

	/**
	 * 获取 web 项目的运行的 WEB-INF 目录
	 * 
	 * @return String
	 */
	public static String getWebInfDir() {
		return getClassesDir().substring(0, getClassesDir().indexOf("classes"));
	}

	/**
	 * 获取项目的编译后或者web项目运行的 classes 目录
	 * <p>
	 * 注意：若项目是 jar 包运行则本方法返回的是""(空串),web 项目和IDE运行的 java 项目才有 classes 目录
	 * 
	 * @return String
	 */
	public static String getClassesDir() {
		return Thread.currentThread().getContextClassLoader().getResource("")
				.getFile();
	}

	/**
	 * 将项目中的文件加载进输入(字节)流中
	 * 
	 * @param filePath
	 *            项目中文件的路径
	 * @return InputStream
	 */
	public static InputStream getResourceAsStream(String filePath) {

		/**
		 * 1.该配置信息是与项目通过 jar 包方式运行时，相关的。
		 * <p>
		 * 注意：###################################
		 * <p>
		 * 若是 maven 项目的 jar 包，则该配置项是 resources/。
		 * <p>
		 * 若是其他自定义的资源文件根路径，则该配置型需要另行修改。如："config/"
		 * <p>
		 * ##########################################
		 */
		String configDir = "resources"+File.separator;
		if (StringUtils.isNotBlank(getClassesDir())) {
			return Thread.currentThread().getContextClassLoader()
					.getResourceAsStream(filePath);
		} else {
			return Thread.currentThread().getContextClassLoader()
					.getResourceAsStream(configDir + filePath);
		}
	}

	/**
	 * 将项目中的文件加载进输入(字符)流中
	 * <p>
	 * 注意：默认取操作系统平台的字符集
	 * 
	 * @param filePath
	 *            项目中文件的路径
	 * @return Reader
	 */
	public static Reader getResourceAsReader(String filePath) {
		return getResourceAsReader(filePath, null);
	}

	/**
	 * 将项目中的文件加载进输入(字符)流中
	 * 
	 * @param filePath
	 *            项目中文件的路径
	 * @param charsetName
	 *            字符流的字符集
	 * @return Reader
	 */
	public static Reader getResourceAsReader(String filePath, String charsetName) {

		Reader reader = null;
		try {
			if (StringUtils.isNotBlank(charsetName)) {
				reader = new InputStreamReader(getResourceAsStream(filePath),
						charsetName);
			} else {
				reader = new InputStreamReader(getResourceAsStream(filePath));
			}

		} catch (UnsupportedEncodingException e) {
			log.error(e.getMessage(), e);
		}
		return reader;
	}

}
