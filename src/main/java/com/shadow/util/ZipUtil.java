package com.shadow.util;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.Date;
import java.util.zip.ZipEntry;
import java.util.zip.ZipOutputStream;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;

import com.shadow.enumation.DateEnumation;

public final class ZipUtil {

	private static Logger log = Logger.getLogger(ZipUtil.class);

	private ZipUtil() {
	}

	/**
	 * 压缩单个文件或者某个目录下所有的文件,不包括子目录
	 * 
	 * @param srcFile
	 *            源：目录或者单个文件
	 * @param zipFileName
	 *            压缩后的文件名称
	 * @return 压缩后文件存储的绝对路径
	 */
	public static String compress(String srcFile, String zipFileName) {
		return compress(new File(srcFile), zipFileName);
	}

	/**
	 * 压缩单个文件或者某个目录下所有的文件,不包括子目录
	 * 
	 * @param srcFile
	 *            源：目录或者单个文件
	 * @param zipFileName
	 *            压缩后的文件名称
	 * @return 压缩后文件存储的绝对路径
	 */
	public static String compress(File srcFile, String zipFileName) {
		return compress(srcFile, zipFileName, false);
	}

	/**
	 * 压缩单个文件或者某个目录下所有的文件,包括子目录中所有的文件，即递归压缩
	 * 
	 * @param srcFile
	 *            源：目录或者单个文件
	 * @param zipFileName
	 *            压缩后的文件名称
	 * @return 压缩后文件存储的绝对路径
	 */
	public static String compressRecursion(String srcFile, String zipFileName) {
		return compressRecursion(new File(srcFile), zipFileName);
	}

	/**
	 * 压缩单个文件或者某个目录下所有的文件,包括子目录中所有的文件，即递归压缩
	 * 
	 * @param srcFile
	 *            源：目录或者单个文件
	 * @param zipFileName
	 *            压缩后的文件名称
	 * @return 压缩后文件存储的绝对路径
	 */
	public static String compressRecursion(File srcFile, String zipFileName) {
		return compress(srcFile, zipFileName, true);
	}

	/**
	 * 压缩单个文件或者某个目录下所有的文件或者某个目录下的所有的文件包括子目录
	 * 
	 * @param srcFile
	 *            源：目录或者单个文件
	 * @param zipFileName
	 *            压缩后的文件名称
	 * @param isRecursion
	 *            是否进行递归压缩
	 * @return 压缩后文件存储的绝对路径
	 * 
	 */
	private static String compress(File srcFile, String zipFileName,
			boolean isRecursion) {

		/*
		 * 1.对文件名称进行严格的判断，若传入则取默认的名称 : yyyyMMHHmmss.zip
		 */
		ZipOutputStream zipOutputStream = null;
		File zipFile = null;
		if (StringUtils.isNotBlank(zipFileName)) {
			if (!zipFileName.endsWith(".zip")) {
				zipFileName += ".zip";
			}
		} else {
			zipFileName = DateUtil.format(new Date(),
					DateEnumation.YYYYMMDDHHMMSS_FORMAT.getFormat()) + ".zip";
		}

		/*
		 * 2.根据要压缩的源是目录还是文件进行选择压缩的操作方法。
		 */
		if (srcFile.isDirectory()) {

			/*
			 * 将压缩后的文件与压缩流进行关联。
			 */
			if (null == zipOutputStream) {
				try {
					zipFile = new File(srcFile.getAbsolutePath()
							+ File.separator + zipFileName);
					zipOutputStream = new ZipOutputStream(new FileOutputStream(
							zipFile));
				} catch (FileNotFoundException e) {
					log.error(e.getMessage(), e);
				}
			}

			/*
			 * 根据是否需要递归压缩，来选择压缩方式
			 */
			if (isRecursion) { // 压缩该文件夹下的所有文件，包括文件夹
				compressDir(srcFile, zipOutputStream, zipFile);
			} else {// 只压缩该文件夹下的文件，不包括文件夹
				/*
				 * 获取源文件夹下所有的文件，并忽略压缩后的文件。
				 */
				File[] files = srcFile.listFiles();
				for (File file : files) {
					if (file.equals(zipFile)) {
						continue;
					}
					compress(file, zipOutputStream);
				}
			}

		} else {
			/*
			 * 将压缩后的文件与压缩流进行关联，并将源文件进行
			 */
			if (null == zipOutputStream) {
				try {
					zipFile = new File(srcFile.getParent() + File.separator
							+ zipFileName);
					zipOutputStream = new ZipOutputStream(new FileOutputStream(
							zipFile));
				} catch (FileNotFoundException e) {
					log.error(e.getMessage(), e);
				}
			}
			compress(srcFile, zipOutputStream);
		}

		/*
		 * 关闭资源
		 */
		try {
			if (null != zipOutputStream) {
				zipOutputStream.flush();
				zipOutputStream.closeEntry();
				zipOutputStream.close();
			}
		} catch (IOException e) {
			log.error(e.getMessage(), e);
		}

		return zipFile.getAbsolutePath();
	}

	/**
	 * 压缩目录下的所有文件，包括其子目录中的文件
	 * 
	 * @param srcFile
	 *            要进行压缩的目录
	 * @param zipOutputStream
	 *            压缩输出流
	 * @param zipFile
	 *            压缩后的文件
	 */
	private static void compressDir(File srcFile,
			ZipOutputStream zipOutputStream, File zipFile) {

		if (srcFile.isDirectory()) {
			File[] listFiles = srcFile.listFiles();
			for (File file : listFiles) {
				if (file.equals(zipFile)) {
					continue;
				}
				if (file.isDirectory()) {
					compressDir(file, zipOutputStream, zipFile);
				} else {
					compress(file, zipOutputStream);
				}
			}
		} else {
			compress(srcFile, zipOutputStream);
		}
	}

	/**
	 * 压缩单个文件
	 * 
	 * @param file
	 *            要进行压缩的文件
	 * @param zipOutputStream
	 *            压缩输出流
	 */
	private static void compress(File file, ZipOutputStream zipOutputStream) {

		/*
		 * 1.若文件存在则进行文件的压缩
		 */
		if (file.exists()) {

			InputStream inputStream = null;
			try {
				/*
				 * 2.将要压缩的文件与输入流进行关联，并将文件名称和zip实体进行关联
				 */
				inputStream = new FileInputStream(file);
				zipOutputStream.putNextEntry(new ZipEntry(file.getName()));

				/*
				 * 3. 读取文件的内容,并通过输出流将文件写到 zip 实体中，实现文件压缩过程
				 */
				IOUtil.write(inputStream, zipOutputStream);
			} catch (FileNotFoundException e) {
				log.error(e.getMessage(), e);
			} catch (IOException e) {
				log.error(e.getMessage(), e);
			} finally {

				/*
				 * 4.关闭资源,注意：zipOutputStream不在此处进行关闭
				 */
				IOUtil.close(inputStream);
			}

		}
	}
}
