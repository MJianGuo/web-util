package com.shadow.util;

import java.lang.annotation.Annotation;
import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.List;

import javassist.CannotCompileException;
import javassist.ClassPool;
import javassist.CtClass;
import javassist.CtField;
import javassist.NotFoundException;
import javassist.bytecode.AnnotationsAttribute;
import javassist.bytecode.ConstPool;
import javassist.bytecode.FieldInfo;
import javassist.bytecode.annotation.StringMemberValue;

import com.shadow.classloader.BeanClassLoader;

/**
 * 关于注解操作的工具类
 * 
 * @author MJG
 * 
 */
public final class AnnotationUtil {

	private AnnotationUtil() {
	}

	/**
	 * 获取指定 beanClazz 类的实例对象中字段上指定的 annotationClazz 注解类型的注解
	 * 
	 * @param beanClazz
	 *            要进行获取其注解类类型
	 * @param annotationClazz
	 *            要进行获取的注解的注解类型
	 * @return List<Annotation> beanClazz 类中字段上的全部的注解
	 * @throws NotFoundException
	 * @throws CannotCompileException
	 */
	public static List<Annotation> getFieldAnnotation(Class<?> beanClazz,
			Class<? extends Annotation> annotationClazz)
			throws NotFoundException, CannotCompileException {

		/*
		 * 1.格式化注解信息
		 */
		Class<? extends Object> tClazz = formatAnnotation(beanClazz,
				annotationClazz);

		/*
		 * 2.获取该类中所有字段
		 */
		Field[] declaredFieldArr = tClazz.getDeclaredFields();
		int length = declaredFieldArr.length;
		List<Annotation> fieldAnnotationList = null;
		if (0 == length) {
			return fieldAnnotationList;
		}
		fieldAnnotationList = new ArrayList<Annotation>(length);

		/*
		 * 3.遍历字段获取字段上指定类型的注解
		 */
		Annotation annotation;
		Field declaredField;
		for (int i = 0; i < length; i++) {
			declaredField = declaredFieldArr[i];
			annotation = declaredField.getAnnotation(annotationClazz);
			if (null != annotation) {
				fieldAnnotationList.add(annotation);
			}
		}

		return fieldAnnotationList;
	}

	/**
	 * 格式化指定类上指定注解的信息，主要是将一些默认的属性值进行补全
	 * 
	 * @param beanClazz
	 *            要进行格式化的类
	 * @param annotationClazz
	 *            要进行补全的注解类
	 * @return Class<?> 补全注解后的类文件
	 * @throws NotFoundException
	 * @throws CannotCompileException
	 */
	private static Class<?> formatAnnotation(Class<?> beanClazz,
			Class<? extends Annotation> annotationClazz)
			throws NotFoundException, CannotCompileException {

		/*
		 * 1. 定义返回值类型
		 */
		Class<?> returnClazz;
		/*
		 * 2. 获取要修改的类的所有信息
		 */
		ClassPool classPool = ClassPool.getDefault();
		CtClass ctClass = classPool.get(beanClazz.getName());
		ctClass.defrost();

		/*
		 * 3.获取该类中所有的字段信息，并进行 fieldName 属性值进行格式化
		 */
		CtField[] declaredFields = ctClass.getDeclaredFields();
		declaredFields = formatAnnotationField(annotationClazz, declaredFields);

		/*
		 * 4.通过自定义的类加载器，从新加载修改后的类文件
		 */
		BeanClassLoader beanClassLoader = new BeanClassLoader(
				BeanClassLoader.class.getClassLoader());
		returnClazz = ctClass.toClass(beanClassLoader, null);

		return returnClazz;
	}

	/**
	 * 格式化注解字段注解上边的 fieldName 属性值，将其值设置为默认的字段的属性名称
	 * 
	 * @param annotationClazz
	 *            要进行格式化的注解类型
	 * @param declaredFields
	 *            某类中所有的字段
	 * @return CtField[] 格式化后的字段
	 * @throws NotFoundException
	 */
	private static CtField[] formatAnnotationField(
			Class<? extends Annotation> annotationClazz,
			CtField[] declaredFields) throws NotFoundException {

		/*
		 * 1.声明遍历到的当前的字段
		 */
		CtField ctField;
		FieldInfo fieldInfo;
		ConstPool filedConstPool;
		for (int i = 0, fieldsLength = declaredFields.length; i < fieldsLength; i++) {

			/*
			 * 2.获取当前遍历到的字段以及字段信息
			 */
			ctField = declaredFields[i];
			fieldInfo = ctField.getFieldInfo();
			filedConstPool = fieldInfo.getConstPool();

			/*
			 * 3. 获取该字段上的注解信息，若该字段上没有注解则继续遍历下一个字段
			 */
			AnnotationsAttribute annotationsAttribute = (AnnotationsAttribute) fieldInfo
					.getAttribute(AnnotationsAttribute.visibleTag);
			if (null == annotationsAttribute) {
				continue;
			}
			/*
			 * 4.从获取的注解属性中获取指定类型的注解，若没有指定类型的注解则继续遍历下一个字段
			 */
			javassist.bytecode.annotation.Annotation annotation = annotationsAttribute
					.getAnnotation(annotationClazz.getName());
			if (null == annotation) {
				continue;
			}
			/*
			 * 5. 获取注解中 fieldName 的值，为 null 或者为 ##default， 则将该值设置为 属性字段的值
			 */
			StringMemberValue fieldNameMemberValue = (StringMemberValue) annotation
					.getMemberValue("fieldName");
			if (null == fieldNameMemberValue
					|| "##default".equals(fieldNameMemberValue)) {
				/*
				 * 6.将注解中 fieldName 的值 修改为本字段的名称
				 */
				annotation.addMemberValue("fieldName", new StringMemberValue(
						ctField.getName(), filedConstPool));
				annotationsAttribute.setAnnotation(annotation);
				fieldInfo.addAttribute(annotationsAttribute);
			}
			/*
			 * 7. 获取注解中 fieldProperty 的值，为 null 或者为 ##default， 则将该值设置为该字段的属性值
			 */
			StringMemberValue fieldPropertyMemberValue = (StringMemberValue) annotation
					.getMemberValue("fieldProperty");
			if (null == fieldPropertyMemberValue
					|| "##default".equals(fieldPropertyMemberValue)) {
				/*
				 * 8.将注解中 fieldProperty 的值 修改为本字段的属性值
				 */
				annotation.addMemberValue("fieldProperty",
						new StringMemberValue(ctField.getType().getName(),
								filedConstPool));
				annotationsAttribute.setAnnotation(annotation);
				fieldInfo.addAttribute(annotationsAttribute);
			}

		}

		return declaredFields;
	}

}
