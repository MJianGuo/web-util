package com.shadow.util;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.lang.annotation.Annotation;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.List;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;

import javassist.CannotCompileException;
import javassist.NotFoundException;

import org.apache.log4j.Logger;
import org.apache.poi.hssf.usermodel.HSSFCell;
import org.apache.poi.hssf.usermodel.HSSFDateUtil;
import org.apache.poi.hssf.usermodel.HSSFRow;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;

import com.shadow.annotation.ExcelAnnotation;
import com.shadow.callable.ExcelCallable;
import com.shadow.model.ExcelModel;
import com.shadow.model.ExcelSheetModel;

/**
 * 操作 excel 文件的工具类
 * 
 * @author MJG
 * 
 */
public final class ExcelUtil {

	private static Logger log = Logger.getLogger(ExcelUtil.class);

	private ExcelUtil() {
	}

	/**
	 * 根据 excelModel 创建对应的excel 文件，并返回该文件的存储路径
	 * 
	 * @param excelModel
	 *            装有 excel 文件内容的自定义对象
	 * @return String 该 excel 文件存储的绝对路径
	 */
	public static <T> ByteArrayOutputStream createExcelToMemory(List<T> excelDataList) {
		return createExcel(excelDataList, null);
	}

	/**
	 * 根据指定的 T 类的集合，以及excel 文件的存储路径和excel 名称生成一个excel 文件，
	 * <p>
	 * 将 List<T>中的数据作为 excel 文件的内容.
	 * <p>
	 * 注意： 若只想将 T 类中，指定的属性字段所对应的数据进行导出到excel中，则字段上上必须有 ExcelAnnotation 注解。
	 * <p>
	 * 否则默认将 T 类中全部字段对应的数据导出到 excel 中
	 * 
	 * @param tList
	 *            要进行导出的数据集合
	 * @param dirPath
	 *            导出后的 excel 文件的存储路径
	 * @param excelName
	 *            导出后的excel 文件的名称
	 * @return 生成的 excel 文件的存储的绝对路径
	 */
	public static <T> String createExcel(List<T> excelDataList, String dirPath, String excelName) {
		return createExcel(excelDataList, dirPath, excelName, null);
	}

	/**
	 * 根据指定的 T 类的集合，以及excel 文件的存储路径和excel 名称生成一个excel 文件，并根据指定的每个 sheet
	 * 工作表单所包含的行数，决定本次生成的 excel 文件要包括集合 sheet 表单
	 * <p>
	 * 将 List<T>中的数据作为 excel 文件的内容.
	 * <p>
	 * 注意： 若只想将 T 类中，指定的属性字段所对应的数据进行导出到excel中，则字段上上必须有 ExcelAnnotation 注解。
	 * <p>
	 * 否则默认将 T 类中全部字段对应的数据导出到 excel 中
	 * 
	 * @param tList
	 *            要进行导出的数据集合
	 * @param dirPath
	 *            导出后的 excel 文件的存储路径
	 * @param excelName
	 *            导出后的excel 文件的名称
	 * @param sheetRowNum
	 *            每个sheet 表单中包含的行数
	 * @return 生成的 excel 文件的存储的绝对路径
	 */
	public static <T> String createExcel(List<T> excelDataList, String dirPath, String excelName, int sheetRowNum) {

		/*
		 * 1.传入 0 则会出现异常，所以本次直接return null
		 */
		if (0 == sheetRowNum) {
			return null;
		}
		/*
		 * 2.根据每个 sheet 表单中的行数，计算要生成的sheet 表单的个数
		 */
		int sheetNum = MathUtil.subListByElementNum(excelDataList, sheetRowNum).size();
		String[] sheetNameArr = new String[sheetNum];
		for (int i = 0; i < sheetNum; i++) {
			sheetNameArr[i] = "";
		}

		return createExcel(excelDataList, dirPath, excelName, sheetNameArr);
	}

	/**
	 * 根据指定的 T 类的集合，以及excel 文件的存储路径和excel 名称生成一个excel 文件，
	 * <p>
	 * 将 List<T>中的数据作为 excel 文件的内容.
	 * <p>
	 * 根据 sheetNameArr 的长度将 excelDataList 中的数据进行切分成指定长度的sheet 表单中
	 * <p>
	 * 注意： 若只想将 T 类中，指定的属性字段所对应的数据进行导出到excel中，则字段上上必须有 ExcelAnnotation 注解。
	 * <p>
	 * 否则默认将 T 类中全部字段对应的数据导出到 excel 中
	 * 
	 * @param tList
	 *            要进行导出的数据集合
	 * @param dirPath
	 *            导出后的 excel 文件的存储路径
	 * @param excelName
	 *            导出后的excel 文件的名称
	 * @param sheetName
	 *            指定 excel 中的sheetName 的名称
	 * @return 生成的 excel 文件的存储的绝对路径
	 */
	public static <T> String createExcel(List<T> excelDataList, String dirPath, String excelName,
			String[] sheetNameArr) {

		/*
		 * 1.若要导出的数据集合是空的，则返回 null，方法体进行终止
		 */
		T t = null;
		if (null != excelDataList && !excelDataList.isEmpty()) {
			t = excelDataList.get(0);
		} else {
			return null;
		}

		/*
		 * 2.获取 T 类中的关于 ExcelAnnotation 的注解内容
		 */
		List<ExcelAnnotation> excelAnnotationList = getBeanExcelAnnotation(t.getClass());
		int excelAnnotationSize = excelAnnotationList.size();
		String[] rowHeaderNameArr = new String[excelAnnotationSize];
		String[] rowHeaderPropertyArr = new String[excelAnnotationSize];
		ExcelAnnotation excelAnnotation = null;
		String rowHeader = null;
		String propertyName = null;
		for (int i = 0; i < excelAnnotationSize; i++) {
			excelAnnotation = excelAnnotationList.get(i);
			rowHeader = excelAnnotation.rowHeader();
			rowHeaderNameArr[i] = rowHeader;
			propertyName = excelAnnotation.fieldName();
			rowHeaderPropertyArr[i] = propertyName;
		}

		/*
		 * 3.创建 excel 文件对应的自定义的 excelModel 对象，并将 sheet 表单的内容添加到该 model 中,根据
		 * sheet 表单的个数初始化 excelModel 中的 sheet 集合的 size, sheet 表单个个数由方法中传递进来的
		 * sheetNameArr 的数组长度进行决定
		 */
		ExcelModel<T> excelModel = null;
		int sheetLength = 0;
		ExcelSheetModel<T> excelSheetModel = null;
		if ((null != sheetNameArr) && (0 != (sheetLength = sheetNameArr.length))) {

			excelModel = new ExcelModel<T>(sheetLength, dirPath, excelName);
			List<List<T>> sheetList = MathUtil.subListToCopies(excelDataList, sheetLength);
			List<T> innerSheet = null;
			String sheetName = null;
			for (int i = 0; i < sheetLength; i++) {
				innerSheet = sheetList.get(i);
				sheetName = sheetNameArr[i];
				excelSheetModel = new ExcelSheetModel<T>(rowHeaderNameArr, rowHeaderPropertyArr, innerSheet);
				excelSheetModel.setSheetName(sheetName);
				excelModel.addSheet(excelSheetModel);
			}
		} else {
			excelModel = new ExcelModel<T>(1, dirPath, excelName);
			excelSheetModel = new ExcelSheetModel<T>(rowHeaderNameArr, rowHeaderPropertyArr, excelDataList);
			excelModel.addSheet(excelSheetModel);
		}

		/*
		 * 5.通过写的底层的解析 自定义的 excelModel 的方法生成对应的 excel 文件，并返回该文件的存储路径
		 */
		return createExcel(excelModel);
	}

	/**
	 * 获取 T 类型的实例对象中字段上的 ExcelAnnotation 注解
	 * 
	 * @param t
	 *            要进行获取器注解的 T 类型的实例对象
	 * @return List<ExcelAnnotation> 该类中字段上的 ExcelAnnotation 注解的集合，顺序是按照注解中的
	 *         orderNum 进行自然数排序的
	 */
	private static List<ExcelAnnotation> getBeanExcelAnnotation(Class<?> clazz) {

		/*
		 * 1.获取字段上的所有的 ExcelAnnotation 类型的注解
		 */
		List<Annotation> fieldAnnotation = null;
		try {
			fieldAnnotation = AnnotationUtil.getFieldAnnotation(clazz, ExcelAnnotation.class);
		} catch (NotFoundException e) {
			log.error(e.getMessage(), e);
		} catch (CannotCompileException e) {
			log.error(e.getMessage(), e);
		}

		/*
		 * 2.遍历注解进行注解类型的转型
		 */
		List<ExcelAnnotation> excelAnnotationList = new ArrayList<ExcelAnnotation>(fieldAnnotation.size());
		ExcelAnnotation excelAnnotation = null;
		for (Annotation annotation : fieldAnnotation) {
			excelAnnotation = (ExcelAnnotation) annotation;
			excelAnnotationList.add(excelAnnotation);
		}

		/*
		 * 3.将转型后的注解进行排序
		 */
		Collections.sort(excelAnnotationList, new Comparator<ExcelAnnotation>() {
			@Override
			public int compare(ExcelAnnotation annotation1, ExcelAnnotation annotation2) {
				return annotation1.orderNum() - annotation2.orderNum();
			}
		});

		return excelAnnotationList;
	}

	/**
	 * 根据 excelModel 创建对应的excel 文件，并返回该文件的存储路径
	 * 
	 * @param excelModel
	 *            装有 excel 文件内容的自定义对象
	 * @return String 该 excel 文件存储的绝对路径
	 */
	private static <T> String createExcel(ExcelModel<T> excelModel) {

		/*
		 * 1.获取该excel文件的存储目录，并进行目录的创建
		 */
		String dirPath = excelModel.getDirPath();
		FileUtil.createDirs(dirPath);

		/*
		 * 2.获取该 excel 文件的名字
		 */
		String excelName = excelModel.getExcelName();

		/*
		 * 3.创建线程池用于存储执行创建 excel 文件的线程，并将该线程提交进行执行，最后关闭线程池等待执行结果
		 */
		ExecutorService executorService = Executors.newFixedThreadPool(1);
		Future<HSSFWorkbook> hSSFWorkFuture = executorService.submit(new ExcelCallable<T>(excelModel));
		executorService.shutdown();

		/*
		 * 4.从线程的执行结果中获取返回的 excel 文件，并将该文件通过输出流写到该文件对应的目录中
		 */
		HSSFWorkbook hSSFWorkbook = null;
		OutputStream outputStream = null;
		File excelFile = null;
		try {
			hSSFWorkbook = hSSFWorkFuture.get();
			if (null != hSSFWorkbook) {
				excelFile = new File(dirPath, excelName);
				outputStream = new FileOutputStream(excelFile);
				hSSFWorkbook.write(outputStream);
			}
		} catch (FileNotFoundException e) {
			log.error(e.getMessage(), e);
		} catch (InterruptedException e) {
			log.error(e.getMessage(), e);
		} catch (ExecutionException e) {
			log.error(e.getMessage(), e);
		} catch (IOException e) {
			log.error(e.getMessage(), e);
		} finally {
			if (null != outputStream) {
				try {
					outputStream.close();
				} catch (IOException e) {
					log.error(e.getMessage(), e);
				}
			}
		}

		return excelFile.getAbsolutePath();
	}

	/**
	 * 根据指定的 T 类的集合，以及excel 文件的存储路径和excel 名称生成一个excel 文件，并根据指定的每个 sheet
	 * 工作表单所包含的行数，决定本次生成的 excel 文件要包括集合 sheet 表单
	 * <p>
	 * 将 List<T>中的数据作为 excel 文件的内容.
	 * <p>
	 * 注意： 若只想将 T 类中，指定的属性字段所对应的数据进行导出到excel中，则字段上上必须有 ExcelAnnotation 注解。
	 * <p>
	 * 否则默认将 T 类中全部字段对应的数据导出到 excel 中
	 * 
	 * @param tList
	 *            要进行导出的数据集合
	 * @param dirPath
	 *            导出后的 excel 文件的存储路径
	 * @param excelName
	 *            导出后的excel 文件的名称
	 * @param sheetRowNum
	 *            每个sheet 表单中包含的行数
	 * @return 生成的 excel 文件的存储的绝对路径
	 */
	public static <T> ByteArrayOutputStream createExcel(List<T> excelDataList, int sheetRowNum) {

		/*
		 * 1.传入 0 则会出现异常，所以本次直接return null
		 */
		if (0 == sheetRowNum) {
			return null;
		}
		/*
		 * 2.根据每个 sheet 表单中的行数，计算要生成的sheet 表单的个数
		 */
		int sheetNum = MathUtil.subListByElementNum(excelDataList, sheetRowNum).size();
		String[] sheetNameArr = new String[sheetNum];
		for (int i = 0; i < sheetNum; i++) {
			sheetNameArr[i] = "";
		}

		return createExcel(excelDataList, sheetNameArr);
	}

	/**
	 * 根据指定的 T 类的集合，以及excel 文件的存储路径和excel 名称生成一个excel 文件，
	 * <p>
	 * 将 List<T>中的数据作为 excel 文件的内容.
	 * <p>
	 * 根据 sheetNameArr 的长度将 excelDataList 中的数据进行切分成指定长度的sheet 表单中
	 * <p>
	 * 注意： 若只想将 T 类中，指定的属性字段所对应的数据进行导出到excel中，则字段上上必须有 ExcelAnnotation 注解。
	 * <p>
	 * 否则默认将 T 类中全部字段对应的数据导出到 excel 中
	 * 
	 * @param tList
	 *            要进行导出的数据集合
	 * @param dirPath
	 *            导出后的 excel 文件的存储路径
	 * @param excelName
	 *            导出后的excel 文件的名称
	 * @param sheetName
	 *            指定 excel 中的sheetName 的名称
	 * @return 生成的 excel 文件的存储的绝对路径
	 */
	public static <T> ByteArrayOutputStream createExcel(List<T> excelDataList, String[] sheetNameArr) {

		/*
		 * 1.若要导出的数据集合是空的，则返回 null，方法体进行终止
		 */
		T t = null;
		if (null != excelDataList && !excelDataList.isEmpty()) {
			t = excelDataList.get(0);
		} else {
			return null;
		}

		/*
		 * 2.获取 T 类中的关于 ExcelAnnotation 的注解内容
		 */
		List<ExcelAnnotation> excelAnnotationList = getBeanExcelAnnotation(t.getClass());
		int excelAnnotationSize = excelAnnotationList.size();
		String[] rowHeaderNameArr = new String[excelAnnotationSize];
		String[] rowHeaderPropertyArr = new String[excelAnnotationSize];
		ExcelAnnotation excelAnnotation = null;
		String rowHeader = null;
		String propertyName = null;
		for (int i = 0; i < excelAnnotationSize; i++) {
			excelAnnotation = excelAnnotationList.get(i);
			rowHeader = excelAnnotation.rowHeader();
			rowHeaderNameArr[i] = rowHeader;
			propertyName = excelAnnotation.fieldName();
			rowHeaderPropertyArr[i] = propertyName;
		}

		/*
		 * 3.创建 excel 文件对应的自定义的 excelModel 对象，并将 sheet 表单的内容添加到该 model 中,根据
		 * sheet 表单的个数初始化 excelModel 中的 sheet 集合的 size, sheet 表单个个数由方法中传递进来的
		 * sheetNameArr 的数组长度进行决定
		 */
		ExcelModel<T> excelModel = null;
		int sheetLength = 0;
		ExcelSheetModel<T> excelSheetModel = null;
		if ((null != sheetNameArr) && (0 != (sheetLength = sheetNameArr.length))) {

			excelModel = new ExcelModel<T>(sheetLength, null, null);
			List<List<T>> sheetList = MathUtil.subListToCopies(excelDataList, sheetLength);
			List<T> innerSheet = null;
			String sheetName = null;
			for (int i = 0; i < sheetLength; i++) {
				innerSheet = sheetList.get(i);
				sheetName = sheetNameArr[i];
				excelSheetModel = new ExcelSheetModel<T>(rowHeaderNameArr, rowHeaderPropertyArr, innerSheet);
				excelSheetModel.setSheetName(sheetName);
				excelModel.addSheet(excelSheetModel);
			}
		} else {
			excelModel = new ExcelModel<T>(1, null, null);
			excelSheetModel = new ExcelSheetModel<T>(rowHeaderNameArr, rowHeaderPropertyArr, excelDataList);
			excelModel.addSheet(excelSheetModel);
		}

		/*
		 * 5.通过写的底层的解析 自定义的 excelModel 的方法生成对应的 excel 文件，并返回该文件的存储路径
		 */
		return createExcelToMemory(excelModel);
	}

	/**
	 * 根据 excelModel 创建对应的excel 文件，并返回该文件的存储路径
	 * 
	 * @param excelModel
	 *            装有 excel 文件内容的自定义对象
	 * @return String 该 excel 文件存储的绝对路径
	 */
	private static <T> ByteArrayOutputStream createExcelToMemory(ExcelModel<T> excelModel) {

		/*
		 * 3.创建线程池用于存储执行创建 excel 文件的线程，并将该线程提交进行执行，最后关闭线程池等待执行结果
		 */
		ExecutorService executorService = Executors.newFixedThreadPool(1);
		Future<HSSFWorkbook> hSSFWorkFuture = executorService.submit(new ExcelCallable<T>(excelModel));
		executorService.shutdown();

		/*
		 * 4.从线程的执行结果中获取返回的 excel 文件，并将该文件通过输出流写到该文件对应的目录中
		 */
		HSSFWorkbook hSSFWorkbook = null;
		ByteArrayOutputStream byteArrayOutputStream = null;
		try {
			hSSFWorkbook = hSSFWorkFuture.get();
			if (null != hSSFWorkbook) {
				byteArrayOutputStream = new ByteArrayOutputStream();
				hSSFWorkbook.write(byteArrayOutputStream);
			}
		} catch (FileNotFoundException e) {
			log.error(e.getMessage(), e);
		} catch (InterruptedException e) {
			log.error(e.getMessage(), e);
		} catch (ExecutionException e) {
			log.error(e.getMessage(), e);
		} catch (IOException e) {
			log.error(e.getMessage(), e);
		}

		return byteArrayOutputStream;
	}

	/**
	 * 根据 excelModel 的集合创建对应的多个 excel 文件，并返回该文件的存储路径
	 * 
	 * @param excelModelList
	 *            装有 excel 文件内容的自定义对象的集合
	 * @return List<String> 生成的多个 excel 文件存储的绝对路径
	 */
	public static <T> List<String> createExcels(List<ExcelModel<T>> excelModelList) {

		/*
		 * 此方法的方法体就是循环执行了 createExcel 中的方法
		 */
		int excelModelListSize = excelModelList.size();
		List<String> excelDirList = new ArrayList<String>(excelModelListSize);
		List<String> excelNameList = new ArrayList<String>(excelModelListSize);
		List<Future<HSSFWorkbook>> hSSFWorkbookFutureList = new ArrayList<Future<HSSFWorkbook>>(excelModelListSize);
		ExcelModel<T> excelModel = null;
		String excelDirStr = null;
		String excelNameStr = null;

		/*
		 * 遍历要生成的 excel ，存储每个文件的名称，以及每个文件任务的返回值，每个任务的返回值就是一个 excel 文件
		 */
		ExecutorService executorService = Executors.newFixedThreadPool(excelModelListSize);
		Future<HSSFWorkbook> hSSFWorkFuture = null;
		for (int i = 0; i < excelModelListSize; i++) {
			excelModel = excelModelList.get(i);
			excelDirStr = excelModel.getDirPath();
			FileUtil.createDirs(excelDirStr);
			excelDirList.add(excelDirStr);
			excelNameStr = excelModel.getExcelName();
			excelNameList.add(excelNameStr);
			hSSFWorkFuture = executorService.submit(new ExcelCallable<>(excelModel));
			hSSFWorkbookFutureList.add(hSSFWorkFuture);
		}
		executorService.shutdown();

		/*
		 * 循环任务的返回结果，将excel 文件写到各个文件对应的目录中
		 */
		List<String> excelPathList = new ArrayList<String>(excelModelListSize);
		HSSFWorkbook hSSFWorkbook = null;
		OutputStream outputStream = null;
		File excelFile = null;
		for (int i = 0; i < excelModelListSize; i++) {
			excelDirStr = excelDirList.get(i);
			excelNameStr = excelNameList.get(i);
			try {
				hSSFWorkbook = hSSFWorkbookFutureList.get(i).get();
				if (null != hSSFWorkbook) {
					excelFile = new File(excelDirStr, excelNameStr);
					outputStream = new FileOutputStream(excelFile);
					hSSFWorkbook.write(outputStream);
					excelPathList.add(excelFile.getAbsolutePath());
				}
			} catch (FileNotFoundException e) {
				log.error(e.getMessage(), e);
			} catch (InterruptedException e) {
				log.error(e.getMessage(), e);
			} catch (ExecutionException e) {
				log.error(e.getMessage(), e);
			} catch (IOException e) {
				log.error(e.getMessage(), e);
			} finally {
				if (null != outputStream) {
					try {
						outputStream.close();
					} catch (IOException e) {
						log.error(e.getMessage(), e);
					}
				}
			}

		}

		return excelPathList;
	}

	// //////////////////////////////////////////////////////////
	/*
	 * 上边是生成 excel 的代码，下边是解析 excel 的代码
	 */
	// //////////////////////////////////////////////////////////

	/**
	 * 将excel 文件的内容读取到指定的类型的对象中，忽略每个 sheet 表单的第一行，即表头
	 * 
	 * @param clazz
	 *            进行数据封装的类类型
	 * @param excelFilePath
	 *            excel 文件的存储路径
	 * @return List<T> 存储有excel 文件数据的集合
	 */
	public static <T> List<T> readExcelToBean(Class<? extends T> clazz, String excelFilePath) {

		/*
		 * 1.将excel文件加载进流中，并与 HSSFWorkbook excel 文件的封装类进行关联
		 */
		File excelFile = new File(excelFilePath);
		HSSFWorkbook hSSFWorkbook = null;
		InputStream inputStream = null;
		try {
			inputStream = new FileInputStream(excelFile);
			hSSFWorkbook = new HSSFWorkbook(inputStream);
		} catch (FileNotFoundException e) {
			log.error(e.getMessage(), e);
		} catch (IOException e) {
			log.error(e.getMessage(), e);
		} finally {
			if (null != inputStream) {
				try {
					inputStream.close();
				} catch (IOException e) {
					log.error(e.getMessage(), e);
				}
			}

		}

		/*
		 * 2.对关联的 excel 文件的封装类进行非空判断
		 */
		if (null == hSSFWorkbook) {
			return null;
		}

		/*
		 * 3.获取该 excel 文件中所有的sheet 表单
		 */
		List<HSSFSheet> hssfSheetList = getHSSFSheet(hSSFWorkbook);

		/*
		 * 4.遍历 sheet 表单进行数据封装
		 */
		HSSFSheet hssfSheet = null;
		List<T> beanList = new ArrayList<T>();
		for (int i = 0, size = hssfSheetList.size(); i < size; i++) {

			hssfSheet = hssfSheetList.get(i);
			beanList.addAll(readSheetToBean(clazz, hssfSheet));
		}

		return beanList;
	}

	/**
	 * 将 sheet 表单中的数据读取到指定的对象中,并忽略sheet 表单的第一行，即表头
	 * 
	 * @param t
	 *            用于接收sheet 表单中数据的对象
	 * @param hssfSheet
	 *            要进行读取的sheet 表单
	 * @return List<T>
	 */
	private static <T> List<T> readSheetToBean(Class<? extends T> clazz, HSSFSheet hssfSheet) {
		return readSheetToBean(clazz, hssfSheet, true);
	}

	/**
	 * 将 sheet 表单中的数据读取到指定的对象中
	 * 
	 * @param t
	 *            用于接收sheet 表单中数据的对象
	 * @param hssfSheet
	 *            要进行读取的sheet 表单
	 * @param ignoreRowHeader
	 *            是否忽略sheet 表单的表头，即一行
	 * @return List<T>
	 */
	private static <T> List<T> readSheetToBean(Class<? extends T> clazz, HSSFSheet hssfSheet, boolean ignoreRowHeader) {

		/*
		 * 1.获取该 sheet 中的行并进行严格的判断
		 */
		int begin = 0;
		List<HSSFRow> hssfRowList = getHSSFRow(hssfSheet);
		if (null == hssfRowList || hssfRowList.isEmpty()) {
			return null;
		}
		/*
		 * 2.是否忽略表头
		 */
		if (ignoreRowHeader) {
			begin = 1;
		}

		return readSheetToBean(clazz, begin, hssfRowList);
	}

	/**
	 * 将 sheet 表单中的数据读取到指定的对象中,并指定从 sheet 表单的第几行开始进行解析数据封装bean
	 * 
	 * @param t
	 *            用于接收sheet 表单中数据的对象
	 * @param begin
	 *            开始解析的行号
	 * @param hssfRowList
	 *            该sheet 表单中的总共的行
	 * @return List<T>
	 */
	private static <T> List<T> readSheetToBean(Class<? extends T> clazz, int begin, List<HSSFRow> hssfRowList) {

		/*
		 * 1.获取总行数以及对象所对应的类中关于excel 的注解信息
		 */
		int hssfRowSize = hssfRowList.size();
		List<T> beanList = new ArrayList<T>(hssfRowSize);
		List<ExcelAnnotation> excelAnnotationList = getBeanExcelAnnotation(clazz);
		int fieldsSize = excelAnnotationList.size();

		/*
		 * 2.通过注解信息获取与 每行数据所对应的属性名称的属性的类型。 -- 注意此时属性类型暂无用到
		 */
		String[] filedsName = new String[fieldsSize];
		String[] filedsProperty = new String[fieldsSize];
		for (int i = 0; i < fieldsSize; i++) {
			filedsName[i] = excelAnnotationList.get(i).fieldName();
			filedsProperty[i] = excelAnnotationList.get(i).fieldProperty();
		}

		/*
		 * 3.遍历 sheet 表单中的数据行进行数据封装
		 */
		T bean = null;
		HSSFRow hssfRow = null;
		String[] fieldsValue = null;
		List<HSSFCell> hssfCellList = null;
		for (; begin < hssfRowSize; begin++) {

			/*
			 * 4.获取遍历到的本行数据
			 */
			hssfRow = hssfRowList.get(begin);
			hssfCellList = getHSSFCell(hssfRow);
			if (null == hssfCellList || hssfCellList.isEmpty()) {
				continue;
			}

			int hssfCellSize = hssfCellList.size();
			fieldsValue = new String[hssfCellSize];
			for (int i = 0; i < hssfCellSize; i++) {
				fieldsValue[i] = getCellValueAsString(hssfCellList.get(i));
			}

			/*
			 * 5.实例化对象
			 */
			try {
				bean = clazz.newInstance();
			} catch (InstantiationException e) {
				log.error(e.getMessage(), e);
			} catch (IllegalAccessException e) {
				log.error(e.getMessage(), e);
			}

			/*
			 * 6.将获取到的数据设置到实例的对象中
			 */
			try {
				ReflectUtil.setProperty(bean, filedsName, fieldsValue);
			} catch (NoSuchFieldException e) {
				log.error(e.getMessage(), e);
			} catch (SecurityException e) {
				log.error(e.getMessage(), e);
			} catch (IllegalArgumentException e) {
				log.error(e.getMessage(), e);
			} catch (IllegalAccessException e) {
				log.error(e.getMessage(), e);
			}

			beanList.add(bean);
		}

		return beanList;
	}

	/**
	 * 获取指定的 excel 文件中，全部的 sheet 工作表单
	 * 
	 * @param hSSFWorkbook
	 *            要进行获取的excel 文件所关联的工作薄对象
	 * @return List<HSSFSheet> 该 excel 文件下的所有的 sheet 工作表单
	 */
	private static List<HSSFSheet> getHSSFSheet(HSSFWorkbook hSSFWorkbook) {

		/*
		 * 1.获取该 excel 中可用的 sheet 工作表单的数量，并根据获取的数量创建用于存储该execel 文件中的sheet
		 * 工作表单的集合
		 */
		int sheetNum = hSSFWorkbook.getNumberOfSheets();
		List<HSSFSheet> hSSFSheetList = new ArrayList<HSSFSheet>(sheetNum);
		HSSFSheet hSSFSheet = null;
		/*
		 * 2.遍历该excel 文件所有可用的 sheet 工作表单，并将表单添加到集合中
		 */
		for (int i = 0; i < sheetNum; i++) {
			hSSFSheet = hSSFWorkbook.getSheetAt(i);
			hSSFSheetList.add(hSSFSheet);
		}

		return hSSFSheetList;
	}

	/**
	 * 获取指定的 excel 文件中，指定角标的的 sheet 工作表单
	 * 
	 * @param hSSFWorkbook
	 *            要进行获取的excel 文件所关联的工作薄对象
	 * @param index
	 *            要获取的 sheet 表单在 excel 中的角标值，
	 *            <p>
	 *            注意：该值是从 0 开始的
	 * @return HSSFSheet
	 */
	public static HSSFSheet getHSSFSheetAtIndex(HSSFWorkbook hSSFWorkbook, int index) {

		/*
		 * 1.获取该 excel 中可用的 sheet 工作表单最大的角标值
		 */
		int activeSheetNum = hSSFWorkbook.getActiveSheetIndex();
		HSSFSheet hSSFSheet = null;
		/*
		 * 2.若传入的角标值在最大的角标值得范围内则根据角标值获取相应的 sheet 工作表单
		 */
		if (index <= activeSheetNum) {
			hSSFSheet = hSSFWorkbook.getSheetAt(index);
		}

		return hSSFSheet;
	}

	/**
	 * 获取指定的 excel 文件中，指定名称的 sheet 工作表单
	 * 
	 * @param hSSFWorkbook
	 *            要进行获取的excel 文件所关联的工作薄对象
	 * @param sheetName
	 *            要进行获取的 sheet 工作表单的名称
	 * @return HSSFSheet
	 */
	public static HSSFSheet getHSSFSheetByName(HSSFWorkbook hSSFWorkbook, String sheetName) {

		return hSSFWorkbook.getSheet(sheetName);
	}

	/**
	 * 获取指定的 excel 中某个指定的 sheet 工作表单中所有的包含数据的行
	 * 
	 * @param hSSFSheet
	 *            excel 文件中某个 sheet 表单关联的对象
	 * @return List<HSSFRow> 该 sheet 工作表单中所有的可用数据行
	 */
	private static List<HSSFRow> getHSSFRow(HSSFSheet hSSFSheet) {

		/*
		 * 1.获取该 sheet 工作表单中含有数据行的数量，并根据获取的数量创建用于存储该sheet 工作表单中数据行的 集合
		 */
		int rowNum = (hSSFSheet.getLastRowNum() + 1);
		List<HSSFRow> hSSFRowList = new ArrayList<HSSFRow>(rowNum);
		HSSFRow hSSFRow = null;
		/*
		 * 2.遍历该 sheet 工作表单中所有可用的数据行，并将行添加到集合中
		 */
		for (int i = 0; i < rowNum; i++) {
			hSSFRow = hSSFSheet.getRow(i);
			hSSFRowList.add(hSSFRow);
		}

		return hSSFRowList;
	}

	/**
	 * 获取指定的 excel 中某个指定的 sheet 工作表单中所有的包含数据的行
	 * 
	 * @param hSSFSheet
	 *            excel 文件中某个 sheet 表单关联的对象
	 * @return List<HSSFRow> 该 sheet 工作表单中所有的可用数据行
	 */
	/**
	 * 获取指定的 excel 中某个指定的 sheet 工作表单中，指定行号的行
	 * 
	 * @param hSSFSheet
	 *            excel 文件中某个 sheet 表单关联的对象
	 * @param rowNum
	 *            sheet 表单中的行号
	 *            <p>
	 *            注意：该行号是从 0 开始的
	 * @return HSSFRow
	 */
	public static HSSFRow getHSSFRowAtRowNum(HSSFSheet hSSFSheet, int rowNum) {

		/*
		 * 1.获取该 sheet 工作表单中最大的行号
		 */
		int lastRowNum = hSSFSheet.getLastRowNum();
		HSSFRow hSSFRow = null;
		/*
		 * 2.若传递进来的行号在最大的行号范围之内则获取指定行号的行
		 */
		if (rowNum <= lastRowNum) {
			hSSFRow = hSSFSheet.getRow(rowNum);
		}

		return hSSFRow;
	}

	/**
	 * 获取 excel 中某行中，所有可用的数据 单元格
	 * 
	 * @param hSSFRow
	 *            excel 中的某个行
	 * @return List<HSSFCell> 该行中所有可用的数据单元格
	 */
	public static List<HSSFCell> getHSSFCell(HSSFRow hSSFRow) {

		short lastCellNum = hSSFRow.getLastCellNum();
		List<HSSFCell> hSSFCellList = new ArrayList<HSSFCell>(lastCellNum);
		HSSFCell hSSFCell = null;
		for (int i = 0; i < lastCellNum; i++) {
			hSSFCell = hSSFRow.getCell(i);
			hSSFCellList.add(hSSFCell);
		}

		return hSSFCellList;
	}

	/**
	 * 获取 excel 中指定某行中，指定角标的可用的 单元格
	 * <p>
	 * 注意：若给定的角标 index 大于本行中可用的单元格的角标则返回 null.角标以 0 开始
	 * 
	 * @param hSSFRow
	 *            excel 中的某个行
	 * @param index
	 *            要获取的单元格在本行中的角标值
	 * @return HSSFCell 返回可用的单元格
	 */
	public static HSSFCell getHSSFCellAtIndex(HSSFRow hSSFRow, int index) {

		short lastCellNum = hSSFRow.getLastCellNum();
		HSSFCell hSSFCell = null;
		if (index <= lastCellNum) {
			hSSFCell = hSSFRow.getCell(index);
		}

		return hSSFCell;
	}

	/**
	 * 将指定单元格中的数据转成字符串进行返回
	 * 
	 * @param hSSFCell
	 *            要进行获取值得单元格
	 * @return String
	 */
	public static String getCellValueAsString(HSSFCell hSSFCell) {

		String cellValue = null;
		if (null == hSSFCell) {
			cellValue = "";
		}
		int cellType = hSSFCell.getCellType();

		if (HSSFCell.CELL_TYPE_STRING == cellType) {
			cellValue = hSSFCell.getStringCellValue();
		} else if (HSSFCell.CELL_TYPE_NUMERIC == cellType) {

			if (HSSFDateUtil.isCellDateFormatted(hSSFCell)) {
				Date date = hSSFCell.getDateCellValue();
				cellValue = DateUtil.format(date);
			} else {
				double numericCellValue = hSSFCell.getNumericCellValue();
				cellValue = String.valueOf(numericCellValue);
			}
		} else if (HSSFCell.CELL_TYPE_BOOLEAN == cellType) {
			boolean booleanCellValue = hSSFCell.getBooleanCellValue();
			cellValue = String.valueOf(booleanCellValue);
		} else {
			cellValue = hSSFCell.getStringCellValue();
		}

		return cellValue;
	}

}
