package com.shadow.util;

import org.springframework.beans.BeansException;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;
import org.springframework.stereotype.Component;

/**
 * sping 容器的工具类，可以通过该类获取spring 容器中装配好的对象
 * <p>
 * 注意：若用该类获取 spring 容器中装配好的对象，需在 spring-xx.xml 配置文件中扫描给类。
 * 
 * @author MJG
 * 
 */
@Component
public final class ApplicationContextUtil implements ApplicationContextAware {

	private ApplicationContextUtil() {
	}

	private static ApplicationContext applicationContext;

	@Override
	public void setApplicationContext(ApplicationContext applicationContext)
			throws BeansException {
		ApplicationContextUtil.applicationContext = applicationContext;
	}

	public static ApplicationContext getApplicationContext() {
		return applicationContext;
	}

	/**
	 * 根据 bean 在spring 容器中的id返回 bean 对象
	 * 
	 * @param beanId
	 * @return
	 */
	public static Object getBean(String beanId) {
		return applicationContext.getBean(beanId);
	}

	/**
	 * 根据 bean 的接口类型和bean 的在spring 容器中的id返回 bean 对象
	 * 
	 * @param beanInterface
	 * @param beanId
	 * @return
	 */
	public static <T> T getBean(Class<? extends T> beanInterface, String beanId) {

		@SuppressWarnings("unchecked")
		T bean = (T) applicationContext.getBean(beanId);
		return bean;
	}

}
