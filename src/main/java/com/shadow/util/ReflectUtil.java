package com.shadow.util;

import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.shadow.annotation.ExcelAnnotation;
import com.shadow.enumation.DateEnumation;
import com.shadow.model.FieldProperty;

/**
 * 这是关于反射相关的工具类
 * 
 * @author MJG
 * 
 */
public final class ReflectUtil {

	// private static Logger log = Logger.getLogger(ReflectUtil.class);

	private ReflectUtil() {
	}

	/**
	 * 将指定的属性和属性对应的值设置到其所属的对象中
	 * 
	 * @param t
	 *            要进行属性值设置的对象
	 * @param filedsName
	 *            对象中的属性字段名称
	 * @param fieldsValue
	 *            与对应中属性名称对应的值
	 * @return
	 * @throws IllegalAccessException
	 * @throws InvocationTargetException
	 * @throws NoSuchMethodException
	 * @throws IllegalArgumentException
	 * @throws SecurityException
	 * @throws NoSuchFieldException
	 */
	public static <T> T setProperty(T t, String[] filedsName,
			String[] fieldsValue) throws NoSuchFieldException,
			SecurityException, IllegalArgumentException, IllegalAccessException {

		/*
		 * 1.对传入的字段名称、字段名称对应的属性、字段名称对应的值进行严格的校验
		 */
		if ((null == filedsName) || (null == fieldsValue)) {
			return null;
		} else if ((filedsName.length == 0)
				|| (filedsName.length != fieldsValue.length)) {
			return null;
		}

		/*
		 * 2.遍历属性名称和与属性名称对应的属性值，将属性值设置到对象中
		 */
		String fieldName = null;
		String fieldValue = null;
		for (int i = 0, length = filedsName.length; i < length; i++) {

			fieldName = filedsName[i];
			fieldValue = fieldsValue[i];
			setProperty(t, fieldName, fieldValue);
		}

		return t;
	}

	/**
	 * 将指定的属性和属性对应的值设置到其所属的对象中
	 * 
	 * @param t
	 *            要进行属性值设置的对象
	 * @param filedsName
	 *            对象中的属性字段名称
	 * @param fieldsValue
	 *            与对应中属性名称对应的值
	 * @return
	 * @throws NoSuchFieldException
	 * @throws SecurityException
	 * @throws IllegalArgumentException
	 * @throws IllegalAccessException
	 */
	public static <T> T setProperty(T t, String filedName, String fieldValue)
			throws NoSuchFieldException, SecurityException,
			IllegalArgumentException, IllegalAccessException {

		/*
		 * 1.获取该对象的类文件，并获取指定的字段
		 */
		Class<?> clazz = t.getClass();
		Field field = clazz.getDeclaredField(filedName);
		if (null == field) {
			return t;
		}

		/*
		 * 2.将私有的字段设置为可以访问的，即暴力访问
		 */
		if (!field.isAccessible()) {
			field.setAccessible(true);
		}

		/*
		 * 3.根据字段类型的类的全类名进行相应的属性值的设定
		 */
		String filedProperty = field.getType().getName();
		if ("short".equals(filedProperty)
				|| "java.lang.Short".equals(filedProperty)) {

			if (fieldValue.contains(".")) {
				fieldValue = fieldValue.substring(0, fieldValue.indexOf("."));
			}
			short value = Short.valueOf(fieldValue);
			field.setShort(t, value);
		} else if ("int".equals(filedProperty)
				|| "java.lang.Integer".equals(filedProperty)) {

			if (fieldValue.contains(".")) {
				fieldValue = fieldValue.substring(0, fieldValue.indexOf("."));
			}
			int value = Integer.valueOf(fieldValue);
			field.setInt(t, value);
		} else if ("long".equals(filedProperty)
				|| "java.lang.Long".equals(filedProperty)) {

			if (fieldValue.contains(".")) {
				fieldValue = fieldValue.substring(0, fieldValue.indexOf("."));
			}
			long value = Long.valueOf(fieldValue);
			field.setLong(t, value);
		} else if ("float".equals(filedProperty)
				|| "java.lang.Float".equals(filedProperty)) {

			float value = Float.valueOf(fieldValue);
			field.setFloat(t, value);
		} else if ("double".equals(filedProperty)
				|| "java.lang.Double".equals(filedProperty)) {

			double value = Double.valueOf(fieldValue);
			field.setDouble(t, value);
		} else if ("byte".equals(filedProperty)
				|| "java.lang.Byte".equals(filedProperty)) {

			byte value = Byte.valueOf(fieldValue);
			field.setByte(t, value);
		} else if ("boolean".equals(filedProperty)
				|| "java.lang.Boolean".equals(filedProperty)) {

			boolean value = Boolean.valueOf(fieldValue);
			field.setBoolean(t, value);
		} else if ("java.util.Date".equals(filedProperty)) {

			ExcelAnnotation excelAnnotation = field
					.getAnnotation(ExcelAnnotation.class);
			String dateFormat = null;
			if (null != excelAnnotation) {
				dateFormat = excelAnnotation.dateFormat();
			} else {
				dateFormat = DateEnumation.DEFAULT_FORMAT.getFormat();
			}

			field.set(t, DateUtil.parse(fieldValue, dateFormat));
		} else {
			field.set(t, fieldValue);
		}

		return t;
	}

	/**
	 * 获取某个对象中指定字段的相关的属性内容以及属性值
	 * 
	 * @param t
	 *            要进行字段属性内容获取的对象
	 * @param fieldName
	 *            要进行属性获取的属性名称
	 * @return FieldProperty 自定义的属性内容的封装类的对象
	 * 
	 * @throws NoSuchFieldException
	 * @throws SecurityException
	 * @throws IllegalArgumentException
	 * @throws IllegalAccessException
	 */
	public static <T> FieldProperty getFieldProperty(T t, String fieldName)
			throws NoSuchFieldException, SecurityException,
			IllegalArgumentException, IllegalAccessException {

		/*
		 * 1.获取该对象所属的类和指定的字段
		 */
		Class<? extends Object> clazz = t.getClass();
		Field field = clazz.getDeclaredField(fieldName);
		if (null == field) {
			return null;
		}

		/*
		 * 2.将私有字段设置为可以被访问，即暴力访问
		 */
		if (!field.isAccessible()) {
			field.setAccessible(true);
		}

		/*
		 * 3.将字段的属性设置到自定义的 字段属性描述对象中，即 FieldProperty
		 */
		FieldProperty fieldProperty = new FieldProperty();

		fieldProperty.setFieldName(fieldName);
		fieldProperty.setModifier(field.getModifiers());
		fieldProperty.setClazz(field.getType());
		fieldProperty.setDeclaringClazz(field.getDeclaringClass());
		fieldProperty.setType(field.getGenericType());
		fieldProperty.setAnnotationArr(field.getAnnotations());
		fieldProperty.setValue(field.get(t));

		return fieldProperty;
	}

	/**
	 * 获取某个类中指定字段的相关的属性内容
	 * 
	 * @param clazz
	 *            某个类
	 * @param fieldName
	 *            该类中的属性名称
	 * @return
	 * @throws NoSuchFieldException
	 * @throws SecurityException
	 * @throws IllegalArgumentException
	 * @throws IllegalAccessException
	 */
	public static FieldProperty getFieldProperty(Class<?> clazz,
			String fieldName) throws NoSuchFieldException, SecurityException,
			IllegalArgumentException, IllegalAccessException {

		/*
		 * 1.获取该对象所属的类和指定的字段
		 */
		Field field = clazz.getDeclaredField(fieldName);
		if (null == field) {
			return null;
		}

		/*
		 * 2.将私有字段设置为可以被访问，即暴力访问
		 */
		if (!field.isAccessible()) {
			field.setAccessible(true);
		}

		/*
		 * 3.将字段的属性设置到自定义的 字段属性描述对象中，即 FieldProperty
		 */
		FieldProperty fieldProperty = new FieldProperty();

		fieldProperty.setFieldName(fieldName);
		fieldProperty.setModifier(field.getModifiers());
		fieldProperty.setClazz(field.getType());
		fieldProperty.setDeclaringClazz(field.getDeclaringClass());
		fieldProperty.setType(field.getGenericType());
		fieldProperty.setAnnotationArr(field.getAnnotations());

		return fieldProperty;
	}

	/**
	 * 获取指定的某个对象中所有字段的属性内容
	 * 
	 * @param t
	 *            指定的某个类的声明类型
	 * @return List<FieldProperty> 该类中所有字段属性的 List 集合
	 * @throws IllegalAccessException
	 * @throws IllegalArgumentException
	 * @throws SecurityException
	 * @throws NoSuchFieldException
	 */
	public static <T> List<FieldProperty> getFieldProperty(T t)
			throws IllegalArgumentException, IllegalAccessException,
			NoSuchFieldException, SecurityException {

		Field[] declaredFieldArr = t.getClass().getDeclaredFields();
		int length = declaredFieldArr.length;
		List<FieldProperty> fieldPropertyList = null;
		if (0 == length) {
			return fieldPropertyList;
		}

		Field field = null;
		String fieldName = null;
		fieldPropertyList = new ArrayList<FieldProperty>(length);
		for (int i = 0; i < length; i++) {

			field = declaredFieldArr[i];
			fieldName = field.getName();
			fieldPropertyList.add(getFieldProperty(t, fieldName));
		}

		return fieldPropertyList;
	}

	/**
	 * 获取指定的某个类中所有字段的属性内容
	 * 
	 * @param clazz
	 *            指定的某个类的声明类型
	 * @return List<FieldProperty> 该类中所有字段属性的 List 集合
	 * @throws IllegalAccessException
	 * @throws IllegalArgumentException
	 * @throws SecurityException
	 * @throws NoSuchFieldException
	 */
	public static List<FieldProperty> getFieldProperty(Class<?> clazz)
			throws NoSuchFieldException, SecurityException,
			IllegalArgumentException, IllegalAccessException {

		Field[] declaredFieldArr = clazz.getDeclaredFields();
		int length = declaredFieldArr.length;
		List<FieldProperty> fieldPropertyList = null;
		if (0 == length) {
			return fieldPropertyList;
		}
		fieldPropertyList = new ArrayList<FieldProperty>(length);
		Field field = null;
		String fieldName = null;
		for (int i = 0; i < length; i++) {
			field = declaredFieldArr[i];
			fieldName = field.getName();
			fieldPropertyList.add(getFieldProperty(clazz, fieldName));
		}

		return fieldPropertyList;
	}

	/**
	 * 获取指定的某个类中所有字段的属性内容
	 * 
	 * @param clazz
	 *            指定的某个类的声明类型
	 * @return Map<String, FieldProperty> 集合 ：
	 *         <p>
	 *         集合的：key --> 字段名称。
	 *         <p>
	 *         集合的：value --> 自定义的 FieldProperty 字段属性封装对象。
	 * @throws IllegalAccessException
	 * @throws IllegalArgumentException
	 * @throws SecurityException
	 * @throws NoSuchFieldException
	 */
	public static Map<String, FieldProperty> getFieldPropertyToMap(
			Class<?> clazz) throws NoSuchFieldException, SecurityException,
			IllegalArgumentException, IllegalAccessException {

		Field[] declaredFieldArr = clazz.getDeclaredFields();
		int length = declaredFieldArr.length;
		Map<String, FieldProperty> fieldMap = null;
		if (0 == length) {
			return fieldMap;
		}

		fieldMap = new HashMap<String, FieldProperty>(length);
		Field field = null;
		String fieldName = null;
		for (int i = 0; i < length; i++) {

			field = declaredFieldArr[i];
			fieldName = field.getName();
			fieldMap.put(fieldName, getFieldProperty(clazz, fieldName));
		}

		return fieldMap;
	}

	/**
	 * 获取指定的对象中所有字段的属性内容
	 * 
	 * @param t
	 *            要进行属性获取的对象
	 * @return Map<String, FieldProperty> 集合 ：
	 *         <p>
	 *         集合的：key --> 字段名称。
	 *         <p>
	 *         集合的：value --> 自定义的 FieldProperty 字段属性封装对象。
	 * @throws NoSuchFieldException
	 * @throws SecurityException
	 * @throws IllegalArgumentException
	 * @throws IllegalAccessException
	 */
	public static <T> Map<String, FieldProperty> getFieldPropertyToMap(T t)
			throws NoSuchFieldException, SecurityException,
			IllegalArgumentException, IllegalAccessException {

		Field[] declaredFieldArr = t.getClass().getDeclaredFields();
		int length = declaredFieldArr.length;
		Map<String, FieldProperty> fieldMap = null;
		if (0 == length) {
			return fieldMap;
		}

		fieldMap = new HashMap<String, FieldProperty>(length);
		Field field = null;
		String fieldName = null;
		for (int i = 0; i < length; i++) {

			field = declaredFieldArr[i];
			fieldName = field.getName();
			fieldMap.put(fieldName, getFieldProperty(t, fieldName));
		}

		return fieldMap;
	}

}
