package com.shadow.util;

import java.io.File;

/**
 * 与操作系统属性配置以及其他硬件信息获取的工具类
 * 
 * @author MJG
 * 
 */
public final class OperaSystemEnvUtil {

	private OperaSystemEnvUtil() {
	}

	/**
	 * 获取操作系统的换行符
	 * <p>
	 * winodws 系统中的换行符是 \r\n
	 * <p>
	 * Unix 系统中的换行符 \n
	 * <p>
	 * Mac 系统中的换行符是 \r
	 * 
	 * @return
	 */
	public static String getLineSeparator() {

		return System.getProperty("line.separator");
	}

	/**
	 * 获取操作系统的文件路径分隔符
	 * <p>
	 * 例如：window 路径分隔符是 \
	 * 
	 * @return
	 */
	public static String getFileSpearator() {

		return File.separator;
	}

	/**
	 * 获取本操作系统的默认字符集编码格式
	 * 
	 * @return
	 */
	public static String getSystemEncoding() {

		return System.getProperty("sun.jnu.encoding");
	}

	/**
	 * 获取操作系统的名称
	 * <p>
	 * 例如 ： Windows 7
	 * 
	 * @return
	 */
	public static String getSystemName() {

		return System.getProperty("os.name");
	}

	/**
	 * 获取当前操作系统使用者所使用的语言
	 * <p>
	 * 例如：中文 zh
	 * 
	 * @return
	 */
	public static String getSystemLanguage() {

		return System.getProperty("user.language");
	}

}
