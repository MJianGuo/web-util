package com.shadow.util;

import java.util.ArrayList;
import java.util.List;

/**
 * 该类是关于数学操作相关的工具类
 * 
 * @author MJG
 * 
 */
public final class MathUtil {

	private MathUtil() {
	}

	/**
	 * 计算机单位
	 */
	private static final String[] COMPUTER_FILE_UNIT = { "B", "KB", "MB", "GB", "TB", "EB", "ZB", "YB", "BB" };

	/**
	 * 将文件的大小计算到合适的单位
	 * 
	 * @param fileByteLength
	 *            文件的大小 B 为单位的值
	 * @return
	 */
	public static String computeFileSize(long fileByteLength) {

		double size = 0.0D;
		size = fileByteLength / 1024;
		int unitIndex = 0;
		if (size == 0.0D) {
			return String.format("%.2f", size) + COMPUTER_FILE_UNIT[unitIndex];
		}
		unitIndex++;
		while (size > 1024) {
			size = size / 1024;
			unitIndex++;
		}
		return String.format("%.2f", size) + COMPUTER_FILE_UNIT[unitIndex];
	}

	/**
	 * 将指定的 sourceList 集合切分成指定的 size 份
	 * 
	 * @param sourceList
	 *            要被切分的集合
	 * @param size
	 *            该集合要被切分成的份数
	 * @return 返回存储有切分后子集合的集合
	 */
	public static <T> List<List<T>> subListToCopies(List<T> sourceList, int size) {

		/*
		 * 1.获取源集合的长度，并根据长度和要被切分成的份数，计算出每份中最多要有几个元素，并根据要被切分的份数创建一个合适的集合，
		 * 用于存储切分后的集合
		 */
		if (0 == size) {// 传入 0 则会出现异常，所以本次直接return null
			return null;
		}
		int sourceListSize = sourceList.size();
		int subListSize = sourceListSize % size == 0 ? sourceListSize / size : ((sourceListSize / size) + 1);
		List<List<T>> resultList = new ArrayList<List<T>>(size);

		/*
		 * 2.定义切分的起始角标，每次开始的角标都是上次结束的角标，每次结束的角标值都是上次结束的角标值加上子集合的 size
		 * 即长度，若最后的结束角标值大于 sourceList 源集合的长度的最大值，则结束角标取 sourceList 源集合的长度
		 */
		int begin = 0;
		int end = 0;
		for (int i = 0; i < size; i++) {

			end += subListSize;
			if (end > sourceListSize) {
				end = sourceListSize;
			}
			resultList.add(sourceList.subList(begin, end));
			begin = end;
		}

		return resultList;
	}

	/**
	 * 将指定的 sourceList 集合切分成每个子集合中都包含有相同个elementNum 的元素数
	 * 
	 * @param sourceList
	 *            要被切分的源集合
	 * @param elementNum
	 *            切分后每个子集合中包含的元素个数
	 * @return 返回存储有切分后子集合的集合
	 */
	public static <T> List<List<T>> subListByElementNum(List<T> sourceList, int elementNum) {

		/*
		 * 1.获取源集合的长度，并根据长度和每个子集合中包含的元素个数，计算出切分后需要集合子集合进行存储，
		 * 并根据子集合的个数创建合适的集合进行存储切分后的子集合
		 */
		if (0 == elementNum) {// 传入 0 则会出现异常，所以本次直接return null
			return null;
		}
		int sourceListSize = sourceList.size();
		int resultListSize = sourceListSize % elementNum == 0 ? sourceListSize / elementNum
				: ((sourceListSize / elementNum) + 1);
		List<List<T>> resultList = new ArrayList<List<T>>(resultListSize);

		/*
		 * 2.定义切分的起始角标，每次开始的角标都是上次结束的角标值，每次结束的角标值就是上次结束的角标值加上每个子元素中应该包含的
		 * elementNum 元素个数，若最后的结束角标值大于 sourceList 源集合的长度的最大值，则结束角标取 sourceList
		 * 源集合的长度
		 */
		int begin = 0;
		int end = 0;
		for (int i = 0; i < resultListSize; i++) {

			end += elementNum;
			if (end > sourceListSize) {
				end = sourceListSize;
			}
			resultList.add(sourceList.subList(begin, end));
			begin = end;
		}

		return resultList;
	}

	/**
	 * 将指定的 sourceList 集合，平均切分成指定的份数，此方法达到了最佳的平均值。
	 * <p>
	 * 注意：每个子集合中的元素的顺序本次是打乱的。
	 * 
	 * @param sourceList
	 *            要被平均切分的源集合
	 * @param size
	 *            要被平均切成指定的份数
	 * @return 返回存储有平均切分后子集合的集合
	 */
	public static <T> List<List<T>> avgList(List<T> sourceList, int size) {

		/*
		 * 1.根据指定的平均的份数，创建相应的存储切分后的子集合
		 */
		if (0 == size) {// 传入 0 则会出现异常，所以本次直接return null
			return null;
		}
		List<List<T>> resultList = new ArrayList<List<T>>(size);
		for (int i = 0; i < size; i++) {
			resultList.add(new ArrayList<T>());
		}

		/*
		 * 2.遍历源集合，根据遍历到的角标 i 对 指定的 size 份数进行求余，余数就是本次遍历到的 T 要在 resultList 中的第 i
		 * 个子集合中进行存储
		 */
		int sourceListSize = sourceList.size();
		int index = 0;
		List<T> subList = null;
		T t = null;
		for (int i = 0; i < sourceListSize; i++) {
			t = sourceList.get(i);
			index = i % size;
			subList = resultList.get(index);
			subList.add(t);
		}

		return resultList;
	}
}
