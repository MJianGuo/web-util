package com.shadow.util;

import java.io.UnsupportedEncodingException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;

/**
 * 该类是用于对明文或者其他字符串进行 MD5 加密的工具类
 * 
 * @author MJG
 */
public final class MD5Util {

	private static Logger log = Logger.getLogger(MD5Util.class);

	private MD5Util() {
	}

	/**
	 * 对明文密码进行 MD5 加密
	 * 
	 * @param obviousPwd
	 *            需要加密的字符串
	 * @return String字符串 加密后的字符串
	 */
	public static String digest(String obviousPwd) {
		return digest(obviousPwd, null);
	}

	/**
	 * 对明文密码进行 MD5 加密
	 * 
	 * @param obviousPwd
	 *            需要加密的字符串
	 * @param solt
	 *            参与加密的盐
	 * @return String字符串 加密后的字符串
	 */
	public static String digest(String obviousPwd, int solt) {
		return digest(digest(obviousPwd) + (digest(solt + "")));
	}

	/**
	 * 对明文密码进行 MD5 加密
	 * 
	 * @param obviousPwd
	 *            需要加密的字符串
	 * @param charsetName
	 *            加密字符串字符集
	 * @param solt
	 *            参与加密的盐
	 * @return String字符串 加密后的字符串
	 */
	public static String digest(String obviousPwd, String charsetName, int solt) {
		return digest(digest(obviousPwd, charsetName) + (digest(solt + "")));
	}

	/**
	 * 获取MD5加密
	 * 
	 * @param obviousPwd
	 *            需要加密的字符串
	 * @param charsetName
	 *            加密字符串字符集
	 * 
	 * @return String字符串 加密后的字符串
	 */
	public static String digest(String obviousPwd, String charsetName) {

		StringBuffer stringBuffer = new StringBuffer();
		try {
			/*
			 * 1.获取 MD5 加密方式的加密对象
			 */
			MessageDigest digest = MessageDigest.getInstance("md5");

			/*
			 * 2. 调用加密对象的方法，对字符串进行加密，加密后结果是一个长度为16的字节数组
			 */
			byte[] digestByte = null;
			if (StringUtils.isNotBlank(charsetName)) {
				digestByte = digest.digest(obviousPwd.getBytes(charsetName));
			} else {
				digestByte = digest.digest(obviousPwd.getBytes());
			}
			for (byte b : digestByte) {
				/*
				 * 3.将加密后的字节数转成0-255之间的正数 。 计算方式：b&255 b:它本来是一个byte类型的数据(1个字节)
				 * 255：是一个int类型的数据(4个字节) byte类型的数据与int类型的数据进行运算，会自动类型提升为int类型
				 * eg: b: 1001 1100(原始数据) 运算时： b: 0000 0000 0000 0000 0000 0000
				 * 1001 1100 255: 0000 0000 0000 0000 0000 0000 1111 1111
				 * 结果：0000 0000 0000 0000 0000 0000 1001 1100 此时的temp是一个int类型的整数
				 */
				int temp = b & 255;

				/*
				 * 4.获取转换后的正数的 16 进制表现形式
				 * 注意：转换的时候注意if正数>=0&&<16，那么如果使用Integer.toHexString()，可能会造成缺少位数
				 */
				if (temp < 16 && temp >= 0) {// 手动补上一个“0”
					stringBuffer.append("0").append(Integer.toHexString(temp));
				} else {
					stringBuffer.append(Integer.toHexString(temp));
				}
			}
		} catch (NoSuchAlgorithmException e) {
			log.error(e.getMessage(), e);
		} catch (UnsupportedEncodingException e) {
			log.error(e.getMessage(), e);
		}
		return stringBuffer.toString();
	}

}
