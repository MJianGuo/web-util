package com.shadow.util;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.PrintWriter;
import java.io.UnsupportedEncodingException;

import javax.servlet.http.HttpServletResponse;

import net.sf.json.JSONArray;
import net.sf.json.JSONObject;

import org.apache.log4j.Logger;

/**
 * 操作 HttpServletResponse 相关的工具类
 * 
 * @author MJG
 * 
 */
public final class ResponseUtil {

	private static Logger log = Logger.getLogger(ResponseUtil.class);

	private ResponseUtil() {
	}


	/**
	 * 将指定的文件写到前台，实现文件下载或导出功能
	 * 
	 * @param response
	 *            响应
	 * @param file
	 *            要进行导出的文件
	 */
	public static void export(HttpServletResponse response, File file) {

		InputStream inputStream = null;
		OutputStream outputStream = null;
		try {

			/*
			 * 1.将文件与输入流进行关联，并将文件读取到缓冲区中
			 */
			inputStream = new BufferedInputStream(new FileInputStream(file));
			byte[] buffer = new byte[inputStream.available()];
			inputStream.read(buffer);

			/*
			 * 2.设置响应头信息
			 */
			response.reset();
			response.setContentType("application/zip");
			response.addHeader("Content-Length", "" + file.length());
			response.setHeader("Content-disposition",
					"attachment;filename="
							+ new String(file.getName().getBytes("UTF-8"),
									"ISO-8859-1"));

			/*
			 * 3.获取响应流，将缓冲区中的内容写到前台
			 */
			outputStream = new BufferedOutputStream(response.getOutputStream());
			outputStream.write(buffer);
			outputStream.flush();
		} catch (FileNotFoundException e) {
			log.error(e.getMessage(), e);
		} catch (UnsupportedEncodingException e) {
			log.error(e.getMessage(), e);
		} catch (IOException e) {
			log.error(e.getMessage(), e);
		} finally {
			/*
			 * 4.关闭资源
			 */
			IOUtil.close(outputStream, inputStream);
		}
	}

	/**
	 * 将json数据写到前台
	 * 
	 * @param response
	 *            响应对象
	 * @param jsonArray
	 *            json数组
	 */
	public static void write(HttpServletResponse response, JSONArray jsonArray) {
		write(response, jsonArray.toString());
	}

	/**
	 * 将json数据写到前台
	 * 
	 * @param response
	 *            响应对象
	 * @param jsonObject
	 *            json对象
	 */
	public static void write(HttpServletResponse response, JSONObject jsonObject) {
		write(response, jsonObject.toString());
	}

	/**
	 * 将json数据写到前台
	 * 
	 * @param response
	 *            响应对象
	 * @param json
	 *            json字符串
	 */
	public static void write(HttpServletResponse response, String json) {

		response.setContentType("text/xml;charset=UTF-8");
		response.setHeader("Pragma", "no-cache");
		response.setHeader("Cache-Control", "no-cache");
		response.setHeader("Expires", "0");
		response.setCharacterEncoding("UTF-8");

		PrintWriter writer = null;
		try {
			writer = response.getWriter();
			writer.write(json);
		} catch (IOException e) {
			log.error(e.getMessage(), e);
		} finally {
			IOUtil.close(writer);
		}
	}
}
