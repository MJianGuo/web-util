package com.shadow.util;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.Reader;
import java.io.UnsupportedEncodingException;
import java.net.MalformedURLException;
import java.util.Properties;

import org.apache.log4j.Logger;

/**
 * 关于操作 *.properties 文件的工具类
 * 
 * @author MJG
 * 
 */
public final class PropertiesUtil {

	private static Logger log = Logger.getLogger(PropertiesUtil.class);

	private PropertiesUtil() {
	}

	/**
	 * 将项目中的 properties 加载进 Properties 集合
	 * 
	 * @param propertiesFilePath
	 *            项目中Properties 配置文件的路径
	 * @return
	 */
	public static Properties loadProperties(String propertiesFilePath) {

		Properties properties = null;
		Reader reader = null;
		try {
			reader = SystemUtil
					.getResourceAsReader(propertiesFilePath, "UTF-8");
			properties = new Properties();
			properties.load(reader);
		} catch (MalformedURLException e) {
			log.error(e.getMessage(), e);
		} catch (UnsupportedEncodingException e) {
			log.error(e.getMessage(), e);
		} catch (FileNotFoundException e) {
			log.error(e.getMessage(), e);
		} catch (IOException e) {
			log.error(e.getMessage(), e);
		} finally {
			IOUtil.close(reader);
		}

		return properties;
	}

}
