package com.shadow.util;

import java.io.File;
import java.io.IOException;
import java.util.Date;

import org.apache.log4j.Logger;

import com.shadow.enumation.DateEnumation;

/**
 * 操作文件的工具类
 * 
 * @author MJG
 * 
 */
public final class FileUtil {

	private static Logger log = Logger.getLogger(FileUtil.class);

	private FileUtil() {
	}

	/**
	 * 根据文件的具体存储路径判断该文件是否存在
	 * 
	 * @param filePath
	 *            要进行判断的文件的具体存储路径
	 * @return true ：该文件存在。
	 *         <p>
	 *         false：该文件不存在
	 */
	public static boolean isExists(String filePath) {
		return new File(filePath).exists();
	}

	/**
	 * 判断该文件是否存在
	 * 
	 * @param file
	 *            要进行判断的文件的对象
	 * @return true ：该文件存在。
	 *         <p>
	 *         false：该文件不存在
	 */
	public static boolean isExists(File file) {
		return file.exists();
	}

	/**
	 * 根据文件的路径创建该文件
	 * 
	 * @param filePath
	 *            文件的绝对路径
	 * @return true ：创建成功。
	 *         <p>
	 *         false:创建失败。
	 */
	public static boolean createFile(String filePath) {
		return createFile(new File(filePath));
	}

	/**
	 * 根据文件对象创建该文件
	 * 
	 * @param file
	 *            封装有文件具体存储路径的文件对象
	 * @return true ：创建成功。
	 *         <p>
	 *         false:创建失败。
	 */
	public static boolean createFile(File file) {

		/*
		 * 1.判断该文件是否存在，不存在则进行创建
		 */
		if (!file.exists()) {
			/*
			 * 2.获取该文件的存储目录，并进行创建
			 */
			if (createDirs(file.getParentFile())) {
				try {
					/*
					 * 3.创建该文件
					 */
					file.createNewFile();
				} catch (IOException e) {
					log.error(e.getMessage(), e);
				}
			}
		}

		return file.exists();
	}

	/**
	 * 创建文件夹,包括多层次的文件夹
	 * 
	 * @param dirPath
	 *            文件夹的具体存储路径
	 * @return true ：创建成功。
	 *         <p>
	 *         false:创建失败。
	 */
	public static boolean createDirs(String dirPath) {
		return createDirs(new File(dirPath));
	}

	/**
	 * 创建文件夹,包括多层次的文件夹
	 * 
	 * @param dir
	 *            封装有文件夹具体存储路径的文件对象
	 * @return true ：创建成功。
	 *         <p>
	 *         false:创建失败。
	 */
	public static boolean createDirs(File dir) {

		/*
		 * 当该目录对象不为空，并且该目录不存在时才创建该目录
		 */
		if ((null != dir) && (!dir.exists())) {
			dir.mkdirs();
		}
		return dir.exists();
	}

	/**
	 * 删除文件夹或文件
	 * 
	 * @param filePath
	 */
	public static boolean deleteFile(String filePath) {
		return deleteFile(new File(filePath));
	}

	/**
	 * 删除文件夹或文件
	 * 
	 * @param file
	 */
	public static boolean deleteFile(File file) {

		if (!file.exists()) {
			return true;
		}

		if (file.isDirectory()) {
			File files[] = file.listFiles();
			for (int i = 0; i < files.length; i++) {
				deleteFile(files[i]);
			}
		}

		file.delete();

		return !file.exists();
	}

	/**
	 * 在指定的父文件夹的基础上创建临时文件夹
	 * 
	 * @param parentDirPath
	 *            父文件夹
	 * @return String 创建好的临时文件夹的绝对路径
	 */
	public static String createTempDir(String parentDirPath) {

		String tempDir = null;
		if (!isExists(parentDirPath)) {
			createDirs(parentDirPath);
		}
		tempDir = parentDirPath
				+ File.separator
				+ DateUtil.format(new Date(),
						DateEnumation.YYYYMMDDHHMMSS_FORMAT.getFormat())
				+ File.separator;

		if (isExists(tempDir)) {
			deleteFile(tempDir);
			createDirs(tempDir);
		} else {
			createDirs(tempDir);
		}

		return tempDir;
	}

}
