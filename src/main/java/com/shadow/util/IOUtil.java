package com.shadow.util;

import java.io.Closeable;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

import org.apache.log4j.Logger;

/**
 * 关于流操作的工具类
 * 
 * @author MJG
 * 
 */
public final class IOUtil {

	private static Logger log = Logger.getLogger(IOUtil.class);

	private static final int BUFFER_SIZE = 4096;

	private IOUtil() {
	}

	/**
	 * 关闭一个或多个流对象
	 * 
	 * @param closeables
	 *            可关闭的流对象列表
	 */
	public static void close(Closeable... closeables) {

		if (closeables != null) {
			for (Closeable closeable : closeables) {
				if (closeable != null) {
					try {
						closeable.close();
					} catch (IOException e) {
						log.error(e.getMessage(), e);
					}
				}
			}
		}
	}

	/**
	 * 将输入中的内容写到输出流中
	 * 
	 * @param inputStream
	 * @param outputStream
	 * @throws IOException
	 */
	public static void write(InputStream inputStream, OutputStream outputStream) {

		int length = 0;
		byte[] buffer = new byte[BUFFER_SIZE];
		try {
			while ((length = inputStream.read(buffer)) != -1) {
				outputStream.write(buffer, 0, length);
			}
		} catch (IOException e) {
			log.error(e.getMessage(), e);
		}
	}

}
