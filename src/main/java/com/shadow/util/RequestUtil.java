package com.shadow.util;

import java.util.Enumeration;

import javax.servlet.http.HttpServletRequest;

/**
 * 操作 HttpServletRequest 的工具类
 * 
 * @author MJG
 * 
 */
public final class RequestUtil {

	private RequestUtil() {
	}

	/**
	 * 获取请求中参数
	 * 
	 * @param request
	 * @return
	 */
	public static String getParameters(HttpServletRequest request) {

		StringBuilder ret = new StringBuilder();
		@SuppressWarnings("unchecked")
		Enumeration<String> keyEnu = request.getParameterNames();
		boolean isFirst = true;
		while (keyEnu.hasMoreElements()) {
			if (isFirst) {
				ret.append("?");
				isFirst = false;
			} else {
				ret.append("&");
			}
			String key = (String) keyEnu.nextElement();
			String value = request.getParameter(key);
			ret.append(key + "=" + value);
		}
		return ret.toString();
	}

	/**
	 * 获取当前请求的url
	 * 
	 * @param request
	 *            请求对象
	 * @return
	 */
	public static String getCurrentURL(HttpServletRequest request) {

		String url = request.getRequestURI();
		String contextPath = request.getContextPath();

		if (url.startsWith(contextPath)) {
			url = url.replaceFirst(contextPath + "/", "");
		}

		return url;
	}

	/**
	 * 获取项目的根地址
	 * 
	 * @param request
	 *            请求对象
	 * @return
	 */
	public String getBaseUrl(HttpServletRequest request) {
		return request.getScheme() + "://" + request.getServerName() + ":"
				+ request.getServerPort() + request.getContextPath();
	}
}
