package com.shadow.main;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.InputStream;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.Properties;

import com.shadow.util.PropertiesUtil;
import com.shadow.util.SystemUtil;

public class Start {

	public static void main(String[] args) throws FileNotFoundException, MalformedURLException {
		
		System.out.println("RootDir -->>>>>>>> "+SystemUtil.getSystemRootDir());
		
		System.out.println(SystemUtil.getResourceAsStream("test/test.properties"));
		System.out.println(SystemUtil.getResourceAsStream("config/config.properties"));
		
		System.out.println("4.1  "+Thread.currentThread().getClass().getResourceAsStream("/test/test.properties"));
		System.out.println("4.2  "+Thread.currentThread().getContextClassLoader().getResourceAsStream("test/test.properties"));
		System.out.println("4.3  "+Thread.currentThread().getClass().getResourceAsStream("/resources/test/test.properties"));
		System.out.println("4.4  "+Thread.currentThread().getContextClassLoader().getResourceAsStream("resources/test/test.properties"));
		/*System.out.println("4  "+Thread.currentThread().getContextClassLoader().getResource("").getFile());
		System.out.println("5  "+Thread.currentThread().getContextClassLoader().getResource("resources/"));
		//System.out.println("5  "+Thread.currentThread().getContextClassLoader().getResource("resources/")+"test/test.properties");
		InputStream resourceAsStream = Thread.currentThread().getContextClassLoader().getResourceAsStream("/resources/test/test.properties");
		System.out.println(resourceAsStream);
		System.out.println("classesDir -->>>>>>>>  "+SystemUtil.getClassesDir());*/
		
		System.out.println("======== PropertiesUtil ============");
		Properties loadPropertiesInJavaEnv = PropertiesUtil.loadProperties("test/test.properties");
		System.out.println(loadPropertiesInJavaEnv);
	}
}
