package com.shadow.annotation;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * 与 excel 文件生成相关的注解类
 * 
 * @author MJG
 * 
 */
@Target({ ElementType.FIELD })
@Retention(RetentionPolicy.RUNTIME)
public @interface ExcelAnnotation {

	/**
	 * 该字段在生成的 excel 文件中列的次序
	 * 
	 * @return
	 */
	int orderNum();

	/**
	 * 该字段在生成的 excel 文件中对应的表头名称
	 * 
	 * @return
	 */
	String rowHeader();

	/**
	 * 该字段的字段名称,默认取当前属性的属性名称
	 * 
	 * @return
	 */
	String fieldName() default "##default";

	/**
	 * 该字段的类型，默认取该字段的类型
	 * 
	 * @return
	 */
	String fieldProperty() default "##default";

	/**
	 * 若本字段是 java.util.Date 日期类型的，该值是其对应的日期格式
	 * 
	 * @return
	 */
	String dateFormat() default "yyyy-MM-dd HH:mm:ss";

}
