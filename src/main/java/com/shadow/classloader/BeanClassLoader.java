package com.shadow.classloader;

/**
 * 自定义的类加载器
 * 
 * @author MJG
 * 
 */
public class BeanClassLoader extends ClassLoader {

	private ClassLoader parent;

	public BeanClassLoader(ClassLoader parent) {
		this.parent = parent;
	}

	@Override
	public Class<?> loadClass(String name) throws ClassNotFoundException {
		return this.loadClass(name, false);
	}

	@Override
	protected synchronized Class<?> loadClass(String name, boolean resolve)
			throws ClassNotFoundException {

		Class<?> clazz = this.findLoadedClass(name);
		if (null != parent) {
			clazz = parent.loadClass(name);
		}
		if (null == clazz) {
			this.findSystemClass(name);
		}
		if (null != clazz && resolve) {
			this.resolveClass(clazz);
		}

		return clazz;
	}

}
