package com.shadow.callable;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.Callable;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;

import org.apache.commons.lang.StringUtils;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;

import com.shadow.model.ExcelModel;
import com.shadow.model.ExcelSheetModel;

/**
 * 根据 excelModel 创建具体的excel 文件的任务类
 * 
 * @author MJG
 * 
 * @param <T>
 */
public class ExcelCallable<T> implements Callable<HSSFWorkbook> {

	/**
	 * 自定义的 ExcelModel 类，该类中描述了该excel 对象的 sheet 表单，以及该excel 的其他信息
	 */
	private ExcelModel<T> excelModel;

	public ExcelCallable(ExcelModel<T> excelModel) {
		this.excelModel = excelModel;
	}

	@Override
	public HSSFWorkbook call() throws Exception {

		/*
		 * 1.创建一个代表 excel 文件的对象
		 */
		HSSFWorkbook hSSFWorkbook = null;
		hSSFWorkbook = new HSSFWorkbook();

		/*
		 * 2.获取该 excel 中要创建的 sheet 工作表单
		 */
		List<ExcelSheetModel<T>> excelSheetList = null;
		excelSheetList = excelModel.getSheetList();

		/*
		 * 3.判断该 excel 中是否有 sheet 工作表单，若没有则直接返回一个空的 excel
		 */
		if (null == excelSheetList || excelSheetList.isEmpty()) {
			hSSFWorkbook.createSheet("sheet1");
			return hSSFWorkbook;
		}

		/*
		 * 4.根据获取的 sheet 表单的数量定义操作 sheet 表单的线程池
		 */
		int excelSheetSize = excelSheetList.size();
		ExcelSheetModel<T> excelSheetModel = null;
		HSSFSheet hSSFSheet = null;
		ExecutorService executorService = null;
		executorService = Executors.newFixedThreadPool(excelSheetSize);

		/*
		 * 5.遍历该 excel 中的 sheet 表单，将每个 sheet 工作表单都放到一个，相对应的任务中去执行
		 */
		String sheetName = null;
		List<Future<HSSFSheet>> hSSFSheetFutureList = new ArrayList<Future<HSSFSheet>>(
				excelSheetSize);
		Future<HSSFSheet> hSSFSheetFuture = null;
		for (int i = 0; i < excelSheetSize; i++) {

			excelSheetModel = excelSheetList.get(i);
			sheetName = excelSheetModel.getSheetName();
			if (StringUtils.isBlank(sheetName)) {
				sheetName = "sheet" + (i + 1);
			}
			hSSFSheet = hSSFWorkbook.createSheet(sheetName);
			hSSFSheetFuture = executorService.submit(new ExcelSheetCallable<T>(
					hSSFSheet, excelSheetModel));
			hSSFSheetFutureList.add(hSSFSheetFuture);
		}

		/*
		 * 6.顺序关闭线程池，不再接受其他的新任务
		 */
		executorService.shutdown();

		/*
		 * 7.在此获取上边关于 sheet 表单的线程执行结果，future.get()
		 * 属于线程阻塞的方法，本方法的执行体会阻塞到这里，等待sheet 表单的生成完毕后再将该 excel 文件对应的对象
		 * hSSFWorkbook 返回给调用者
		 */
		for (Future<HSSFSheet> future : hSSFSheetFutureList) {
			future.get();
		}

		return hSSFWorkbook;
	}

}
