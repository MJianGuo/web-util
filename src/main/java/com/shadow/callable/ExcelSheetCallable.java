package com.shadow.callable;

import java.lang.reflect.InvocationTargetException;
import java.util.Date;
import java.util.List;
import java.util.concurrent.Callable;

import org.apache.poi.hssf.usermodel.HSSFCell;
import org.apache.poi.hssf.usermodel.HSSFRow;
import org.apache.poi.hssf.usermodel.HSSFSheet;

import com.shadow.annotation.ExcelAnnotation;
import com.shadow.enumation.DateEnumation;
import com.shadow.model.ExcelSheetModel;
import com.shadow.model.FieldProperty;
import com.shadow.util.DateUtil;
import com.shadow.util.ReflectUtil;

/**
 * 关于 excel 中某个 sheet 工作表单创建以及内容设置的任务类
 * 
 * @author MJG
 * 
 * @param <T>
 */
public class ExcelSheetCallable<T> implements Callable<HSSFSheet> {

	/**
	 * excel 中某个 sheet 表单对应的类
	 */
	private HSSFSheet hSSFSheet;
	/**
	 * 自定义的 excel 的表单的 sheet 模型，该模型中包含该 sheet 表单中的数据
	 */
	private ExcelSheetModel<T> excelSheetModel;

	public ExcelSheetCallable(HSSFSheet hSSFSheet,
			ExcelSheetModel<T> excelSheetModel) {
		this.hSSFSheet = hSSFSheet;
		this.excelSheetModel = excelSheetModel;
	}

	@Override
	public HSSFSheet call() throws Exception {

		/*
		 * 1. 定义 sheet 表单的第一行，也就是该 sheet 表单的表头行
		 */
		HSSFRow headerRow = null;

		/*
		 * 2. 获取该 sheet 表单中的表头的中文信息，若有中文信息，则创建表头
		 */
		String[] rowHeaderNameArr = null;
		rowHeaderNameArr = excelSheetModel.getRowHeaderNameArr();
		if ((null != rowHeaderNameArr) && (0 != rowHeaderNameArr.length)) {

			headerRow = hSSFSheet.createRow(0);
			/*
			 * 3.创建该表头行的具体的一个单元格，将表头的中文信息，设定到表头行中相应的单元格中
			 */
			HSSFCell headerRowCell = null;
			for (int i = 0, rowHeaderlength = rowHeaderNameArr.length; i < rowHeaderlength; i++) {
				headerRowCell = headerRow.createCell(i);
				headerRowCell.setCellValue(rowHeaderNameArr[i]);
			}
		}

		/*
		 * 4.根据是否已经有表头来判断 excel 下一行的起始值
		 */
		int j;
		if (null != headerRow) {
			j = 1;
		} else {
			j = 0;
		}

		/*
		 * 5.声明除了表头之外的其他行
		 */
		HSSFRow hSSFRow = null;

		/*
		 * 6.获取该 sheet 表单中的内容数据
		 */
		List<T> sheetContentDataList = null;
		sheetContentDataList = excelSheetModel.getSheetContentDataList();
		/*
		 * 7.从 ExcelsheetModel 中，获取要从 T 对象中获取指定数据所对应的的属性名称
		 */
		String[] rowHeaderPropertyArr = null;
		rowHeaderPropertyArr = excelSheetModel.getRowHeaderPropertyArr();

		/*
		 * 8.定义要从数据集合中获取的具体某行数据所对应的 T 对象
		 */
		T t = null;

		/*
		 * 9.遍历 ExcelSheetModel 中 sheet 表单的内容，将内容设置到相应的行中
		 */
		int sheetContentDataSize = (null != sheetContentDataList) ? sheetContentDataList
				.size() : 0;

		/*
		 * 10.若没有指定 T 对象中的属性名称，则默认按照 T 对象中属性全部写到 excel 的 sheet 工作表单中
		 */
		if ((null != rowHeaderPropertyArr)
				&& (0 != rowHeaderPropertyArr.length)) {
			for (int i = 0; i < sheetContentDataSize; i++) {
				t = sheetContentDataList.get(i);
				hSSFRow = hSSFSheet.createRow(j + i);
				setRowCellContent(hSSFRow, t, rowHeaderPropertyArr);
			}
		} else {
			for (int i = 0; i < sheetContentDataSize; i++) {
				t = sheetContentDataList.get(i);
				hSSFRow = hSSFSheet.createRow(j + i);
				setRowCellContent(hSSFRow, t);
			}
		}

		return hSSFSheet;
	}

	/**
	 * 设置 excel 表格中某行中相应的单元格的内容
	 * 
	 * @param hSSFRow
	 *            要进行内容设置的当前的 excel 的行
	 * @param t
	 *            存储有本行内容数据的 T 对象
	 * @return
	 * @throws IllegalAccessException
	 * @throws IllegalArgumentException
	 * @throws SecurityException
	 * @throws NoSuchFieldException
	 */
	private HSSFRow setRowCellContent(HSSFRow hSSFRow, T t)
			throws IllegalArgumentException, IllegalAccessException,
			NoSuchFieldException, SecurityException {

		/*
		 * 1.将该 T 对象中的属性加载到 Map 集合中
		 */

		List<FieldProperty> fieldPropertyList = ReflectUtil.getFieldProperty(t);

		/*
		 * 2.遍历 T 对象中的属性，并获取属性值，然后将属性值赋值到本行的单元格中
		 */
		FieldProperty fieldProperty = null;
		Object value = null;
		HSSFCell hSSFCell = null;
		String valueStr = null;
		for (int i = 0, size = fieldPropertyList.size(); i < size; i++) {
			fieldProperty = fieldPropertyList.get(i);
			value = fieldProperty.getValue();
			if (null != value) {
				valueStr = String.valueOf(value);
			} else {
				valueStr = "";
			}
			hSSFCell = hSSFRow.createCell(i);
			hSSFCell.setCellValue(valueStr);

		}

		return hSSFRow;
	}

	/**
	 * 设置 excel 表格中某行中相应的单元格的内容
	 * 
	 * @param hSSFRow
	 *            要进行内容设置的当前的 excel 的行
	 * @param t
	 *            存储有本行内容数据的 T 对象
	 * @param rowHeaderPropertyArr
	 *            要从 T 对象中获取指定属性数据的，属性名称
	 * @return HSSFRow 写好数据的本行对象
	 * 
	 * @throws IllegalAccessException
	 * @throws InvocationTargetException
	 * @throws NoSuchMethodException
	 * @throws IllegalArgumentException
	 * @throws SecurityException
	 * @throws NoSuchFieldException
	 */
	private HSSFRow setRowCellContent(HSSFRow hSSFRow, T t,
			String[] rowHeaderPropertyArr) throws IllegalAccessException,
			InvocationTargetException, NoSuchMethodException,
			NoSuchFieldException, SecurityException, IllegalArgumentException {

		/*
		 * 1. 遍历 t 类中的属性名称，根据属性名称去类中获取数据，并将数据设置到本行中对应的单元格中
		 */
		Object value = null;
		String valueStr = null;
		String propertyName = null;
		HSSFCell hSSFCell = null;
		FieldProperty fieldProperty = null;
		String dateFormat = null;
		for (int i = 0, rowHeaderPropertyLength = rowHeaderPropertyArr.length; i < rowHeaderPropertyLength; i++) {

			/*
			 * 2.从 t 对象中获取指定属性的值
			 */
			fieldProperty = ReflectUtil.getFieldProperty(t,
					rowHeaderPropertyArr[i]);
			value = fieldProperty.getValue();

			/*
			 * 3.对获取的属性值进行非空判断
			 */
			if (null == value) {
				valueStr = "";
			}

			/*
			 * 4.获取该属性类型的全类名，再根据全类名进行 excel 中单元格的值得设定
			 */
			propertyName = fieldProperty.getClazz().getName();
			hSSFCell = hSSFRow.createCell(i);
			if ("int".equals(propertyName)
					|| "java.lang.Integer".equals(propertyName)) {

				valueStr = String.valueOf(value);
				hSSFCell.setCellType(HSSFCell.CELL_TYPE_NUMERIC);
				hSSFCell.setCellValue(Integer.parseInt(valueStr));
			} else if ("long".equals(propertyName) 
					|| "java.lang.Long".equals(propertyName)){
				valueStr = String.valueOf(value);
				hSSFCell.setCellType(HSSFCell.CELL_TYPE_NUMERIC);
				hSSFCell.setCellValue(Long.parseLong(valueStr));
			}else if ("double".equals(propertyName)
					|| "java.lang.Double".equals(propertyName)) {

				valueStr = String.valueOf(value);
				hSSFCell.setCellType(HSSFCell.CELL_TYPE_NUMERIC);
				hSSFCell.setCellValue(Double.parseDouble(valueStr));
			} else if ("boolean".equals(propertyName)
					|| "java.lang.Boolean".equals(propertyName)) {

				valueStr = String.valueOf(value);
				hSSFCell.setCellType(HSSFCell.CELL_TYPE_BOOLEAN);
				hSSFCell.setCellValue(Boolean.parseBoolean(valueStr));
			} else if ("java.lang.String".equals(propertyName)) {

				valueStr = String.valueOf(value);
				hSSFCell.setCellType(HSSFCell.CELL_TYPE_STRING);
				hSSFCell.setCellValue(valueStr);
			} else if ("java.util.Date".equals(propertyName)) {

				ExcelAnnotation annotation = fieldProperty
						.getAnnotation(ExcelAnnotation.class);
				if (null != annotation) {
					dateFormat = annotation.dateFormat();
				} else {
					dateFormat = DateEnumation.DEFAULT_FORMAT.getFormat();
				}

				valueStr = DateUtil.format((Date) value, dateFormat);
				hSSFCell.setCellValue(valueStr);
			} else {
				valueStr = String.valueOf(value);
				hSSFCell.setCellValue(valueStr);
			}

		}

		return hSSFRow;
	}

}
